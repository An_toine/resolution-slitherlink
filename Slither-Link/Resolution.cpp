//
//  Resolution.cpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 20/02/2019.
//  Copyright © 2019. All rights reserved.
//

#include "Resolution.hpp"

using namespace std;

Resolution::Resolution(Grille &g, Parcours &p) : grille(g), parcours(p), tmp(0) {
    tempsAffichage = 800000;
    affichageCoupParCoup = true;
    affichage();
}

void Resolution::affichage(void){
    if(affichageCoupParCoup){
        cout<<grille<<endl;
        parcours.afficherParcours();
        usleep(tempsAffichage);
        system("clear");
    }

}

bool Resolution::backTracking()
{
    cout << "Lancement du backtracking..." << endl;
    usleep(8000000);
    
    bool bt_running = true;
    Lien* l         = NULL;
    Point* p        = NULL;

    unsigned int id_trace  = 0;
    unsigned int id_point  = 1;
    unsigned int id_lien   = 1;
    unsigned int type_lien;
    unsigned int id_lien_debutBT;

    bool resOpLogiques;
    bool resCasParticulier;

    parcours.sauvegarderParcours(); 

    while(bt_running == true) {
        id_point = 1;
        while(bt_running     && id_point <= 2) {                 // On effectue le backtracking pour les 4 liens d'une extrêmité d'un tracé.
            id_lien = 1;
            p = parcours.retournerPointBT(id_trace, id_point);
            while(bt_running && id_lien <= 4) {                // On effectue le backtracking pour les 2 extrêmités d'un tracé.
                if(p->atL(id_lien)->getType() == Lien::VIDE) {
                    type_lien = 0;
                    while(bt_running && type_lien < 2) {

                        resOpLogiques     = false;
                        resCasParticulier = false;
                        switch(type_lien) { 
                            case 0 : {
                                if((p->atL(id_lien)->atP1()->getNbSegment() <2 && p->atL(id_lien)->atP2()->getNbSegment() < 2))
                                    transformeLien(*p->atL(id_lien), Lien::SEGMENT); 
                                else
                                    bt_running = false;
                                break;
                            }
                            case 1 : transformeLien(*p->atL(id_lien), Lien::CROIX); break;
                        }
                        
                        if(bt_running) {
                            id_lien_debutBT = LiensUtilise.size() - 1;

                            do {
                                resCasParticulier = boucleCasParticulier();
                                resOpLogiques     = boucleOperationsLogiques();
                                if(erreurPoint3Segments(id_lien_debutBT)) {
                                    bt_running = false;
                                }

                            } while(!(resOpLogiques || !(resCasParticulier)) && bt_running);
                            if(parcours.parcoursTermine()) {
                                bt_running = false;
                                return true;
                            }
                        
                            parcours.backTrackParcours();
                            rembobinage(id_lien_debutBT);
                        }
                        ++type_lien;
                    }
                }
                ++id_lien;
            }
            ++id_point;
        }
        ++id_trace;
    }
    return bt_running;
}

void Resolution::rembobinage(unsigned int id_lien)
{
    for(int i = LiensUtilise.size() - 1; i >= id_lien-1; --i) {
        LiensUtilise[i]->setType(Lien::VIDE);
        LiensUtilise[i]->atP1()->update();
        LiensUtilise[i]->atP2()->update();
        LiensUtilise.erase(LiensUtilise.begin() + i);
    }
    
    for(int i = 0; i < grille.recherche.size(); ++i) {
        grille.recherche[i]->update();
    }
}


bool Resolution::erreurPoint3Segments(unsigned int id_lien_debutBT) const
{
    for(int i = LiensUtilise.size()-1; i >= id_lien_debutBT; i--) {
        if(LiensUtilise[i]->atP1()->getNbSegment() > 2 || LiensUtilise[i]->atP2()->getNbSegment() > 2)
            return true;
    }
    return false;
}


bool Resolution::transformeLien(Lien &l, Lien::e_type type)
{
    if(lienDejaUtilise(l)) {
        return false;
    }
    else {
        l.setType(type);
        tmp++;
        if(type == Lien::SEGMENT) {
            if(l.atP1()->getNbSegment() < 2 || l.atP2()->getNbSegment() < 2) { 
                parcours.updateTraces(l.atP1(), l.atP2());
                parcours.updateFusionTraces();
                affichage();
            }
            else
                return false;
        }
    
        l.atP1()->update();
        reglesPoint(*l.atP1());
    
        l.atP2()->update();
        reglesPoint(*l.atP2());
        
        if(erreurPoint(*l.atP1()))
            return false; // retourne false -> signifie que le lien est mauvais
        if(erreurPoint(*l.atP2()))
            return false; // retourne false -> signifie que le lien est mauvais
    
        return true; // retourne true -> signifie que le lien est ok
    }
}


bool Resolution::boucleOperationsLogiques(void){
    
    int nbTourSansChangement=0;
    long int dernierTailleTabRecherche = grille.recherche.size();
    while(grille.recherche.size() && nbTourSansChangement <= 2){
        dernierTailleTabRecherche = grille.recherche.size();
        for(int i=0;i<grille.recherche.size();i++){
            if(!casParticuliersGeneral(*grille.recherche[i]) || grille.recherche[i]->estContrainte()){
                remplirContrainte(*grille.recherche[i]);
                if(grille.recherche[i]->getNbLien()==4){
                    grille.recherche.erase(grille.recherche.begin() + i);
                }
            }
            grille.recherche[i]->update();
        }

        if(dernierTailleTabRecherche == grille.recherche.size()){
            nbTourSansChangement++;
        }else{
            nbTourSansChangement = 0;
        }
    }
    
    if(grille.recherche.size() == 0){
        return true;
    }else{
        return false;
    }

}


bool Resolution::casParticuliersGeneral(Case& _case){
    if(CaseCoin1(_case)){
        return true;
    }else if(CaseCoin2(_case)){
        return true;
    }else if(CaseCoin3(_case)){
        return true;
    }
    return false;
}

bool Resolution::boucleCasParticulierDebut(){
    bool applique = false;
    for(int i=0;i<grille.recherche.size();i++){
        grille.recherche[i]->update();
        
        if(grille.recherche[i]->getValeurContrainte() == 0){
            if(CaseSeul0(*grille.recherche[i])){
                applique = true;}
            
        }else if(grille.recherche[i]->getValeurContrainte() == 1){
            if(CaseCoin1(*grille.recherche[i])){
                applique = true;}
            
        }else if(grille.recherche[i]->getValeurContrainte() == 2){
            if(CaseCoin2(*grille.recherche[i])){
                applique = true;}
            
            //c'est la case contrainte 2 qui est demandé dans le cas particulier
            if(CaseDiag323(*grille.recherche[i])){
                applique = true;}
            
        }else if(grille.recherche[i]->getValeurContrainte() == 3){
            if(CaseCoin3(*grille.recherche[i])){
                applique = true;}
            
            if(CaseVoisin33(*grille.recherche[i])){
                applique = true;}
            if(CaseDiag33(*grille.recherche[i])){
                applique = true;}
        }
        remplirContrainte(*grille.recherche[i]);
        //affichage();
    }
    if(CasSemiBoucle()) {
        applique = true;
    }


    return applique;
}

bool Resolution::boucleCasParticulier(){
    bool applique = false;
    for(int i=0;i<grille.recherche.size();i++){
        grille.recherche[i]->update();

        if(CasSemiBoucle()) {
            applique = true;
        }
        
        if(grille.recherche[i]->getValeurContrainte() == 1){
            
            if(CaseSeul1_1(*grille.recherche[i])){
                applique = true;}
            if(CaseSeul1_2(*grille.recherche[i])){
                applique = true;}
            if(CaseSeul1_1M(*grille.recherche[i])){
                applique = true;}
            
            if(CaseDiag11_1(*grille.recherche[i])){
                applique = true;}
            if(CaseDiag11_2(*grille.recherche[i])){
                applique = true;}
            if(CaseDiag12(*grille.recherche[i])){
                applique = true;}
            
        }else if(grille.recherche[i]->getValeurContrainte() == 2){
            
            if(CaseSeul2(*grille.recherche[i])){
                applique = true;}
            
            if(CaseSeul1_2M(*grille.recherche[i])){
                applique = true;}
            if(CaseSeul2M(*grille.recherche[i])){
                applique = true;}
            
            if(CaseDiag22_1(*grille.recherche[i])){
                applique = true;}
            if(CaseDiag22_2(*grille.recherche[i])){
                applique = true;}
            
            if(CaseDiag223(*grille.recherche[i])){
                applique = true;}
            
        }else if(grille.recherche[i]->getValeurContrainte() == 3){

            if(CaseDiag31_1(*grille.recherche[i])){
                applique = true;}
            if(CaseDiag31_2(*grille.recherche[i])){
                applique = true;}
            
        }
        
        remplirContrainte(*grille.recherche[i]);

        //affichage();
    }
    return applique;
}

void Resolution::reglesPointSurCase(Case &c){
    reglesPoint(*c.atL(1)->atP1());
    reglesPoint(*c.atL(1)->atP2());
    reglesPoint(*c.atL(3)->atP1());
    reglesPoint(*c.atL(3)->atP2());
}

bool Resolution::erreurPoint(Point &p){
    // règles test erreur
    if(p.getNbSegment() == 3){
        // alors il y a erreur
        return true;
    }
    if(p.getNbSegment() == 1 && p.getNbCroix() == 2){
        // alors il y a erreur
        return true;
    }
    return false;
}

void Resolution::reglesPoint(Point &p){
    // règle 1
    if(p.getNbCroix() == 3 && p.getNbVide() == 1){
        // alors lien vide devient lien croix
        while(p.unLienVide() != NULL){
            transformeLien(*p.unLienVide(), Lien::CROIX);
        }
    }
    
    // règle 2
    if(p.getNbSegment() == 2 && (p.getNbVide() == 2 || p.getNbVide() == 1)){
        // alors les deux liens vide deviennent des lien croix
        while(p.unLienVide() != NULL){
            transformeLien(*p.unLienVide(), Lien::CROIX);
        }
    }
    
    // règle 3
    if(p.getNbSegment() == 1 && p.getNbCroix() == 2 && p.getNbVide() == 1){
        // alors les deux liens vide deviennent des lien croix
        while(p.unLienVide() != NULL){
            transformeLien(*p.unLienVide(), Lien::SEGMENT);
        }
    }
}



bool Resolution::remplirContrainte(Case &c){
    reglesPointSurCase(c);
    bool remplir = false;
    if(c.estContrainte()){
        bool remplirContrainteSegment = (c.getValeurContrainte() == (c.getNbSegment() + c.getNbVide()));
        if(remplirContrainteSegment){
            while(c.unLienVide() != NULL){
                this->transformeLien(*c.unLienVide(), Lien::SEGMENT);
            }
            remplir = true;
        }
        
        bool remplirContrainteCroix = (c.getValeurContrainte() == 4 - (c.getNbCroix() + c.getNbVide()));
        if(remplirContrainteCroix){
            while(c.unLienVide() != NULL){
                this->transformeLien(*c.unLienVide(), Lien::CROIX);
            }
            remplir = true;
        }
    }
    return remplir;
}

  // -------------------------------- \\
 // -------- cas particuliers -------- \\
// ------------------------------------ \\

bool Resolution::CaseSeul0(Case &c){
    if(c.getValeurContrainte() == 0){
        if(c.unLienVide() == NULL){
            transformeLien(*c.atL(1), Lien::CROIX);
            transformeLien(*c.atL(2), Lien::CROIX);
            transformeLien(*c.atL(3), Lien::CROIX);
            transformeLien(*c.atL(4), Lien::CROIX);
            c.update();
            return true;
        }
    }
    return false;
    
}

bool Resolution::CaseSeul1_1(Case &c){
    if(c.getValeurContrainte() == 1){
        if(c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP1()->atL(4)->getType() == Lien::SEGMENT){
            if(c.atL(2)->getType() == Lien::VIDE || c.atL(3)->getType() == Lien::VIDE){
                transformeLien(*c.atL(2), Lien::CROIX);
                transformeLien(*c.atL(3), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::SEGMENT && c.atL(1)->atP2()->atL(2)->getType() == Lien::CROIX){
            if(c.atL(3)->getType() == Lien::VIDE || c.atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(3), Lien::CROIX);
                transformeLien(*c.atL(4), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(2)->getType() == Lien::SEGMENT){
            if(c.atL(1)->getType() == Lien::VIDE || c.atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(4), Lien::CROIX);
                transformeLien(*c.atL(1), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP1()->atL(4)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT){
            if(c.atL(1)->getType() == Lien::VIDE || c.atL(2)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1), Lien::CROIX);
                transformeLien(*c.atL(2), Lien::CROIX);
                c.update();
                return true;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseSeul1_1M(Case &c){
    if(c.getValeurContrainte() == 1){
       if(c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(4)->getType() == Lien::SEGMENT){
           if(c.atL(1)->getType() == Lien::VIDE || c.atL(2)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1), Lien::CROIX);
                transformeLien(*c.atL(2), Lien::CROIX);
                c.update();
                return true;
           }
        }else if(c.atL(1)->atP1()->atL(1)->getType() == Lien::SEGMENT && c.atL(1)->atP1()->atL(4)->getType() == Lien::CROIX){
            if(c.atL(2)->getType() == Lien::VIDE || c.atL(3)->getType() == Lien::VIDE){
                transformeLien(*c.atL(2), Lien::CROIX);
                transformeLien(*c.atL(3), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(2)->getType() == Lien::SEGMENT){
            if(c.atL(3)->getType() == Lien::VIDE || c.atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(4), Lien::CROIX);
                transformeLien(*c.atL(3), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP2()->atL(2)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(3)->getType() == Lien::SEGMENT){
            if(c.atL(1)->getType() == Lien::VIDE || c.atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1), Lien::CROIX);
                transformeLien(*c.atL(4), Lien::CROIX);
                c.update();
                return true;
            }
        }else{
            return false;
        }
    }
    return false;
     
}

bool Resolution::CaseSeul1_2(Case &c){
    if(c.getValeurContrainte() == 1){
        if(c.atL(1)->atP1()->atL(4)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::CROIX && c.atL(3)->getType() == Lien::CROIX){
            if(c.atL(1)->atP1()->atL(1)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1)->atP1()->atL(1), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::SEGMENT && c.atL(3)->getType() == Lien::CROIX && c.atL(4)->getType() == Lien::CROIX){
            if(c.atL(1)->atP2()->atL(2)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1)->atP2()->atL(2), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP2()->atL(2)->getType() == Lien::SEGMENT && c.atL(4)->getType() == Lien::CROIX && c.atL(1)->getType() == Lien::CROIX){
            if(c.atL(3)->atP2()->atL(3)->getType() == Lien::VIDE){
                transformeLien(*c.atL(3)->atP2()->atL(3), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT && c.atL(1)->getType() == Lien::CROIX && c.atL(2)->getType() == Lien::CROIX){
            if(c.atL(3)->atP1()->atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(3)->atP1()->atL(4), Lien::CROIX);
                c.update();
                return true;
            }

        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseSeul1_2M(Case &c){
    if(c.getValeurContrainte() == 1){
       if(c.atL(3)->atP1()->atL(4)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::CROIX && c.atL(1)->getType() == Lien::CROIX){
           if(c.atL(3)->atP1()->atL(3)->getType() == Lien::VIDE){
               transformeLien(*c.atL(3)->atP1()->atL(3), Lien::CROIX);
               c.update();
               return true;
           }
        }else if(c.atL(1)->atP1()->atL(1)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::CROIX && c.atL(3)->getType() == Lien::CROIX){
            if(c.atL(1)->atP1()->atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1)->atP1()->atL(4), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(1)->atP2()->atL(2)->getType() == Lien::SEGMENT && c.atL(3)->getType() == Lien::CROIX && c.atL(4)->getType() == Lien::CROIX){
            if(c.atL(1)->atP2()->atL(1)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1)->atP2()->atL(1), Lien::CROIX);
                c.update();
                return true;
            }

        }else if(c.atL(3)->atP2()->atL(3)->getType() == Lien::SEGMENT && c.atL(4)->getType() == Lien::CROIX && c.atL(1)->getType() == Lien::CROIX){
            if(c.atL(3)->atP2()->atL(2)->getType() == Lien::VIDE){
                transformeLien(*c.atL(3)->atP2()->atL(2), Lien::CROIX);
                c.update();
                return true;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseSeul2(Case &c){
    if(c.getValeurContrainte() == 2){
        if(c.atL(4)->atP1()->atL(4)->getType() == Lien::SEGMENT && c.atL(1)->getType() == Lien::CROIX){
            if(c.atL(2)->getType() == Lien::VIDE || c.atL(3)->atP1()->atL(3)->getType() == Lien::VIDE){
                transformeLien(*c.atL(2), Lien::SEGMENT);
                transformeLien(*c.atL(3)->atP1()->atL(3),Lien::CROIX);
                c.update();
                return true;
                
            }
        }else if(c.atL(1)->atP1()->atL(1)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::CROIX){
            if(c.atL(3)->getType() == Lien::VIDE || c.atL(1)->atP1()->atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(3), Lien::SEGMENT);
                transformeLien(*c.atL(1)->atP1()->atL(4),Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(2)->atP1()->atL(2)->getType() == Lien::SEGMENT && c.atL(3)->getType() == Lien::CROIX){
            if(c.atL(3)->getType() == Lien::VIDE || c.atL(1)->atP2()->atL(1)->getType() == Lien::VIDE){
                transformeLien(*c.atL(3), Lien::SEGMENT);
                transformeLien(*c.atL(1)->atP2()->atL(1),Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT && c.atL(4)->getType() == Lien::CROIX){
            if(c.atL(1)->getType() == Lien::VIDE || c.atL(3)->atP2()->atL(2)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1), Lien::SEGMENT);
                transformeLien(*c.atL(3)->atP2()->atL(2),Lien::CROIX);
                c.update();
                return true;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseSeul2M(Case &c){
    if(c.getValeurContrainte() == 2){
        if(c.atL(1)->atP1()->atL(4)->getType() == Lien::SEGMENT && c.atL(3)->getType() == Lien::CROIX){
            if(c.atL(2)->getType() == Lien::VIDE || c.atL(1)->atP1()->atL(1)->getType() == Lien::VIDE){
                transformeLien(*c.atL(2),Lien::SEGMENT);
                transformeLien(*c.atL(1)->atP1()->atL(1),Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::SEGMENT && c.atL(4)->getType() == Lien::CROIX){
            if(c.atL(3)->getType() == Lien::VIDE || c.atL(1)->atP2()->atL(2)->getType() == Lien::VIDE){
                transformeLien(*c.atL(3),Lien::SEGMENT);
                transformeLien(*c.atL(1)->atP2()->atL(2),Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP2()->atL(2)->getType() == Lien::SEGMENT && c.atL(1)->getType() == Lien::CROIX){
            if(c.atL(4)->getType() == Lien::VIDE || c.atL(2)->atP2()->atL(3)->getType() == Lien::VIDE){
                transformeLien(*c.atL(4),Lien::SEGMENT);
                transformeLien(*c.atL(2)->atP2()->atL(3),Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::CROIX){
            if(c.atL(1)->getType() == Lien::VIDE || c.atL(3)->atP1()->atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1),Lien::SEGMENT);
                transformeLien(*c.atL(3)->atP1()->atL(4),Lien::CROIX);
                c.update();
                return true;
            }
        }else{
            return false;
        }
    }
    return false;
}

//CASE COIN

bool Resolution::CaseCoin1(Case &c){
    if(c.getValeurContrainte() == 1){
        if(c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP1()->atL(4)->getType() == Lien::CROIX){
            if(c.atL(1)->getType() == Lien::VIDE || c.atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1), Lien::CROIX);
                transformeLien(*c.atL(4), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(2)->getType() == Lien::CROIX){
            if(c.atL(1)->getType() == Lien::VIDE || c.atL(2)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1), Lien::CROIX);
                transformeLien(*c.atL(2), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP2()->atL(2)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX){
            if(c.atL(2)->getType() == Lien::VIDE || c.atL(3)->getType() == Lien::VIDE){
                transformeLien(*c.atL(2), Lien::CROIX);
                transformeLien(*c.atL(3), Lien::CROIX);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(4)->getType() == Lien::CROIX){
            if(c.atL(3)->getType() == Lien::VIDE || c.atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(3), Lien::CROIX);
                transformeLien(*c.atL(4), Lien::CROIX);
                c.update();
                return true;
            }
        }else{
            return false;
        }
    }
        return false;
    
}

bool Resolution::CaseCoin2(Case &c){
    if(c.getValeurContrainte() == 2){
        if(c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP1()->atL(4)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atL(4)->atP2()->atL(4)->getType() == Lien::CROIX){
            if(c.atL(3)->atP1()->atL(3)->getType() == Lien::VIDE || c.atL(1)->atP2()->atL(2)->getType() == Lien::VIDE){
                transformeLien(*c.atL(3)->atP1()->atL(3), Lien::SEGMENT);
                transformeLien(*c.atL(1)->atP2()->atL(2), Lien::SEGMENT);
                c.update();
                return true;
            }
        }else if(c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atL(2)->atP1()->atL(2)->getType() == Lien::CROIX && c.atL(2)->atP2()->atL(2)->getType() == Lien::CROIX){
            if(c.atL(1)->atP1()->atL(4)->getType() == Lien::VIDE || c.atL(3)->atP2()->atL(3)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1)->atP1()->atL(4), Lien::SEGMENT);
                transformeLien(*c.atL(3)->atP2()->atL(3), Lien::SEGMENT);
                c.update();
                return true;
            }
        }else if(c.atL(2)->atP1()->atL(2)->getType() == Lien::CROIX && c.atL(2)->atP2()->atL(2)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX){
            if(c.atL(1)->atP2()->atL(1)->getType() == Lien::VIDE || c.atL(3)->atP1()->atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1)->atP2()->atL(1), Lien::SEGMENT);
                transformeLien(*c.atL(3)->atP1()->atL(4), Lien::SEGMENT);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX && c.atL(4)->atP1()->atL(4)->getType() == Lien::CROIX && c.atL(4)->atP2()->atL(4)->getType() == Lien::CROIX){
            if(c.atL(1)->atP1()->atL(1)->getType() == Lien::VIDE || c.atL(3)->atP2()->atL(2)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1)->atP1()->atL(1), Lien::SEGMENT);
                transformeLien(*c.atL(3)->atP2()->atL(2), Lien::SEGMENT);
                c.update();
                return true;
            }
        }else{
            return false;
        }
    }
    return false;
}

bool Resolution::CaseCoin3(Case &c){
    if(c.getValeurContrainte() == 3){
        if(c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP1()->atL(4)->getType() == Lien::CROIX){
            if(c.atL(1)->getType() == Lien::VIDE || c.atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1), Lien::SEGMENT);
                transformeLien(*c.atL(4), Lien::SEGMENT);
                c.update();
                return true;
            }
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(2)->getType() == Lien::CROIX){
            if(c.atL(1)->getType() == Lien::VIDE || c.atL(2)->getType() == Lien::VIDE){
                transformeLien(*c.atL(1), Lien::SEGMENT);
                transformeLien(*c.atL(2), Lien::SEGMENT);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP2()->atL(2)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX){
            if(c.atL(2)->getType() == Lien::VIDE || c.atL(3)->getType() == Lien::VIDE){
                transformeLien(*c.atL(2), Lien::SEGMENT);
                transformeLien(*c.atL(3), Lien::SEGMENT);
                c.update();
                return true;
            }
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(4)->getType() == Lien::CROIX){
            if(c.atL(3)->getType() == Lien::VIDE || c.atL(4)->getType() == Lien::VIDE){
                transformeLien(*c.atL(3), Lien::SEGMENT);
                transformeLien(*c.atL(4), Lien::SEGMENT);
                c.update();
                return true;
            }
        }else{
            return false;
        }
    }
    return false;
}




//CASE VOISIN
//---------------------------------------------------------------------

bool Resolution::CaseVoisin33(Case &c){
    if(c.getValeurContrainte() == 3){
        int  i = 1;
        while(i <=7 && c.atCaseVoisin(i)->getValeurContrainte() != 3){
            i=i+2;
        }
        if(i<=7){
            switch(i){
                case 1:
                    if(c.atL(1)->getType() == Lien::VIDE || c.atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(1)->atL(1)->getType() == Lien::VIDE || c.atL(1)->atP1()->atL(4)->getType() == Lien::VIDE || c.atL(1)->atP2()->atL(2)->getType() == Lien::VIDE){
                        transformeLien(*c.atL(1), Lien::SEGMENT);
                        transformeLien(*c.atL(3), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(1)->atL(1), Lien::SEGMENT);
                        transformeLien(*c.atL(1)->atP1()->atL(4), Lien::CROIX);
                        transformeLien(*c.atL(1)->atP2()->atL(2), Lien::CROIX);
                        
                        c.update();
                        c.atCaseVoisin(1)->update();
                        return true;
                    }
                    break;
                case 3:
                    if(c.atL(4)->getType() == Lien::VIDE || c.atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(3)->atL(2)->getType() == Lien::VIDE || c.atL(2)->atP1()->atL(1)->getType() == Lien::VIDE || c.atL(2)->atP2()->atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atL(4), Lien::SEGMENT);
                        transformeLien(*c.atL(2), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(3)->atL(2), Lien::SEGMENT);
                        transformeLien(*c.atL(2)->atP1()->atL(1), Lien::CROIX);
                        transformeLien(*c.atL(2)->atP2()->atL(3), Lien::CROIX);
                        c.update();
                        c.atCaseVoisin(3)->update();
                        return true;
                    }
                    break;
                case 5:
                    if(c.atL(1)->getType() == Lien::VIDE || c.atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(5)->atL(3)->getType() == Lien::VIDE || c.atL(3)->atP1()->atL(4)->getType() == Lien::VIDE || c.atL(3)->atP2()->atL(2)->getType() == Lien::VIDE){
                        transformeLien(*c.atL(1), Lien::SEGMENT);
                        transformeLien(*c.atL(3), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(5)->atL(3), Lien::SEGMENT);
                        transformeLien(*c.atL(3)->atP1()->atL(4), Lien::CROIX);
                        transformeLien(*c.atL(3)->atP2()->atL(2), Lien::CROIX);
                        c.update();
                        c.atCaseVoisin(5)->update();
                        return true;
                    }
                    break;
                case 7:
                    if(c.atL(4)->getType() == Lien::VIDE || c.atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(7)->atL(4)->getType() == Lien::VIDE || c.atL(4)->atP1()->atL(1)->getType() == Lien::VIDE || c.atL(4)->atP2()->atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atL(4), Lien::SEGMENT);
                        transformeLien(*c.atL(2), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(7)->atL(4), Lien::SEGMENT);
                        transformeLien(*c.atL(4)->atP1()->atL(1), Lien::CROIX);
                        transformeLien(*c.atL(4)->atP2()->atL(3), Lien::CROIX);
                        c.update();
                        c.atCaseVoisin(7)->update();
                        return true;
                    }
                    break;
                default :
                    return false;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

//CASE DIAGONALE
// --------------------------------------------------------------------





bool Resolution::CaseDiag11_2(Case &c){
    if(c.getValeurContrainte() == 1){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 1){
                if(i == 2 && c.atL(3)->getType() == Lien::CROIX && c.atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 4 && c.atL(4)->getType() == Lien::CROIX && c.atL(1)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 6 && c.atL(1)->getType() == Lien::CROIX && c.atL(2)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 8 && c.atL(2)->getType() == Lien::CROIX && c.atL(3)->getType() == Lien::CROIX){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
            switch(i){
                case 2:
                    if(c.atCaseVoisin(2)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(2)->atL(2)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(2)->atL(1), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(2)->atL(2), Lien::CROIX);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 4:
                    if(c.atCaseVoisin(4)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(4)->atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(4)->atL(2), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(4)->atL(3), Lien::CROIX);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 6:
                    if(c.atCaseVoisin(6)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(6)->atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(6)->atL(3), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(6)->atL(4), Lien::CROIX);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 8:
                    if(c.atCaseVoisin(8)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(8)->atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(8)->atL(1), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(8)->atL(4), Lien::CROIX);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    }
                    break;

                default :
                    return false;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseDiag11_1(Case &c){
    
    if(c.getValeurContrainte() == 1){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 1){
                if(i == 2 && c.atL(1)->getType() == Lien::CROIX && c.atL(2)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 4 && c.atL(2)->getType() == Lien::CROIX && c.atL(3)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 6 && c.atL(3)->getType() == Lien::CROIX && c.atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 8 && c.atL(1)->getType() == Lien::CROIX && c.atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
            switch(i){
                case 2:
                    if(c.atCaseVoisin(2)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(2)->atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(2)->atL(3), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(2)->atL(4), Lien::CROIX);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 4:
                    if(c.atCaseVoisin(4)->atL(4)->getType() == Lien::VIDE || c.atCaseVoisin(4)->atL(1)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(4)->atL(4), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(4)->atL(1), Lien::CROIX);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 6:
                    if(c.atCaseVoisin(6)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(6)->atL(2)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(6)->atL(1), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(6)->atL(2), Lien::CROIX);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 8:
                    if(c.atCaseVoisin(8)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(8)->atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(8)->atL(2), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(8)->atL(3), Lien::CROIX);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    }
                    break;
                default :
                    return false;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseDiag12(Case &c){ //le 2 est la case centrale c
    if(c.getValeurContrainte() == 2){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 1){
                if(i == 2 && c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(4)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 4 && c.atL(1)->atP1()->atL(4)->getType() == Lien::CROIX && c.atL(1)->atP1()->atL(1)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 6 && c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(2)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 8 && c.atL(3)->atP2()->atL(2)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(3)->getType() == Lien::SEGMENT){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
            switch(i){
                case 2:
                    if(c.atCaseVoisin(2)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(2)->atL(1)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(2)->atL(2), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(2)->atL(1), Lien::CROIX);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 4:
                    if(c.atCaseVoisin(4)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(4)->atL(2)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(4)->atL(3), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(4)->atL(2), Lien::CROIX);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 6:
                    if(c.atCaseVoisin(6)->atL(4)->getType() == Lien::VIDE || c.atCaseVoisin(6)->atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(6)->atL(4), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(6)->atL(3), Lien::CROIX);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 8:
                    if(c.atCaseVoisin(8)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(8)->atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(8)->atL(1), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(8)->atL(4), Lien::CROIX);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    }
                    break;
                default :
                    return false;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseDiag12M(Case &c){ //le 2 est la case centrale c
    if(c.getValeurContrainte() == 2){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 1){
                if(i == 2 && c.atL(3)->atP1()->atL(4)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 4 && c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP1()->atL(4)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 6 && c.atL(1)->atP2()->atL(2)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(1)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 8 && c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(2)->getType() == Lien::SEGMENT){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
            switch(i){
                case 2:
                    if(c.atCaseVoisin(2)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(2)->atL(1)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(2)->atL(2), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(2)->atL(1), Lien::CROIX);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 4:
                    if(c.atCaseVoisin(4)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(4)->atL(2)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(4)->atL(3), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(4)->atL(2), Lien::CROIX);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 6:
                    if(c.atCaseVoisin(6)->atL(4)->getType() == Lien::VIDE || c.atCaseVoisin(6)->atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(6)->atL(4), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(6)->atL(3), Lien::CROIX);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 8:
                    if(c.atCaseVoisin(8)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(8)->atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(8)->atL(1), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(8)->atL(4), Lien::CROIX);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    }
                    break;
                default :
                    return false;
            }
        }else{
            return false;
        }
    }
    return false;
}

bool Resolution::CaseDiag22_1(Case &c){
    if(c.getValeurContrainte() == 2){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 2){
                if(i == 2 && c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT && c.atL(3)->atP1()->atL(4)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 4 && c.atL(1)->atP1()->atL(1)->getType() == Lien::SEGMENT && c.atL(1)->atP1()->atL(4)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 6 && c.atL(1)->atP2()->atL(1)->getType() == Lien::SEGMENT && c.atL(1)->atP2()->atL(2)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 8 && c.atL(3)->atP2()->atL(2)->getType() == Lien::SEGMENT && c.atL(3)->atP2()->atL(3)->getType() == Lien::SEGMENT){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
            switch(i){
                case 2:
                    if(c.atL(2)->getType() == Lien::VIDE || c.atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(2)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(2)->atL(1)->getType() == Lien::VIDE){
                        transformeLien(*c.atL(2), Lien::SEGMENT);
                        transformeLien(*c.atL(1), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(2)->atL(2), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(2)->atL(1), Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(2)->update();
                        return true;
                    }
                    break;
                case 4:
                    if(c.atL(3)->getType() == Lien::VIDE || c.atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(4)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(4)->atL(2)->getType() == Lien::VIDE){
                        transformeLien(*c.atL(3), Lien::SEGMENT);
                        transformeLien(*c.atL(2), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(4)->atL(3), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(4)->atL(2), Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(4)->update();
                        return true;
                    }
                    break;
                case 6:
                    if(c.atL(4)->getType() == Lien::VIDE || c.atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(6)->atL(4)->getType() == Lien::VIDE || c.atCaseVoisin(6)->atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atL(4), Lien::SEGMENT);
                        transformeLien(*c.atL(3), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(6)->atL(4), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(6)->atL(3), Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(6)->update();
                        return true;
                    }
                    break;
                case 8:
                    if(c.atL(1)->getType() == Lien::VIDE || c.atL(4)->getType() == Lien::VIDE || c.atCaseVoisin(8)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(8)->atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atL(1), Lien::SEGMENT);
                        transformeLien(*c.atL(4), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(8)->atL(1), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(8)->atL(4), Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(8)->update();
                        return true;
                    }
                    break;
                default :
                    return false;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseDiag22_2(Case &c){
    if(c.getValeurContrainte() == 2){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 2){
                if(i == 2 && c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT && c.atL(3)->atP1()->atL(4)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(1)->atP2()->atL(1)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 4 && c.atL(1)->atP1()->atL(4)->getType() == Lien::SEGMENT && c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(3)->atP2()->atL(2)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 6 && c.atL(1)->atP2()->atL(1)->getType() == Lien::SEGMENT && c.atL(1)->atP2()->atL(2)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(3)->atP1()->atL(3)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 8 && c.atL(3)->atP2()->atL(2)->getType() == Lien::SEGMENT && c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(1)->atP1()->atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
            switch(i){
                case 2:
                    if(c.atCaseVoisin(2)->atL(1)->atP2()->atL(2)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(2)->atL(1)->atP2()->atL(2), Lien::SEGMENT);
                        c.atCaseVoisin(2)->update();
                        return true;
                    }
                    break;
                case 4:
                    if(c.atCaseVoisin(4)->atL(3)->atP2()->atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(4)->atL(3)->atP2()->atL(3), Lien::SEGMENT);
                        c.atCaseVoisin(4)->update();
                        return true;
                    }
                    break;
                case 6:
                    if(c.atCaseVoisin(6)->atL(1)->atP1()->atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(6)->atL(1)->atP1()->atL(4), Lien::SEGMENT);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 8:
                    if(c.atCaseVoisin(8)->atL(1)->atP1()->atL(1)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(8)->atL(1)->atP1()->atL(1), Lien::SEGMENT);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    }
                    break;
                default :
                    return false;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseDiag22_2M(Case &c){
    if(c.getValeurContrainte() == 2){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 2){
                if(i == 2 && c.atL(3)->atP1()->atL(4)->getType() == Lien::SEGMENT && c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(1)->atP2()->atL(2)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 4 && c.atL(1)->atP1()->atL(1)->getType() == Lien::SEGMENT && c.atL(1)->atP1()->atL(4)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(3)->atP2()->atL(3)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 6 && c.atL(1)->atP2()->atL(2)->getType() == Lien::SEGMENT && c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(3)->atP1()->atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 8 && c.atL(3)->atP2()->atL(3)->getType() == Lien::SEGMENT && c.atL(3)->atP2()->atL(2)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(1)->atP1()->atL(1)->getType() == Lien::CROIX){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
            switch(i){
                case 2:
                    if(c.atCaseVoisin(2)->atL(1)->atP2()->atL(1)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(2)->atL(1)->atP2()->atL(1), Lien::SEGMENT);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 4:
                    if(c.atCaseVoisin(2)->atL(3)->atP2()->atL(2)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(2)->atL(3)->atP2()->atL(2), Lien::SEGMENT);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 6:
                    if(c.atCaseVoisin(3)->atL(1)->atP1()->atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(3)->atL(1)->atP1()->atL(3), Lien::SEGMENT);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 8:
                    if(c.atCaseVoisin(4)->atL(1)->atP1()->atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(4)->atL(1)->atP1()->atL(4), Lien::SEGMENT);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    }
                    break;
                default :
                    return false;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseDiag31_1(Case &c){
    if(c.getValeurContrainte() == 3){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 1){// la case 3 est donc la case centrale...
                if(i == 2 && c.atL(4)->getType() == Lien::SEGMENT && c.atL(3)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 4 && c.atL(1)->getType() == Lien::SEGMENT && c.atL(4)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 6 && c.atL(2)->getType() == Lien::SEGMENT && c.atL(1)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 8 && c.atL(3)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::SEGMENT){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
            switch(i){
                case 2:
                    if(c.atCaseVoisin(2)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(2)->atL(2)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(2)->atL(1), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(2)->atL(2), Lien::CROIX);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 4:
                    if(c.atCaseVoisin(4)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(4)->atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(4)->atL(2), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(4)->atL(3), Lien::CROIX);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 6:
                    if(c.atCaseVoisin(6)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(6)->atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(6)->atL(3), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(6)->atL(4), Lien::CROIX);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 8:
                    if(c.atCaseVoisin(8)->atL(4)->getType() == Lien::VIDE || c.atCaseVoisin(8)->atL(1)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(8)->atL(4), Lien::CROIX);
                        transformeLien(*c.atCaseVoisin(8)->atL(1), Lien::CROIX);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    }
                    break;
                default :
                    return false;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseDiag31_2(Case &c){
    if(c.getValeurContrainte() == 1){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 3){// la case 1 est donc la case centrale...
                if(i == 2 && c.atL(3)->getType() == Lien::CROIX && c.atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 4 && c.atL(4)->getType() == Lien::CROIX && c.atL(1)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 6 && c.atL(1)->getType() == Lien::CROIX && c.atL(2)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 8 && c.atL(2)->getType() == Lien::CROIX && c.atL(3)->getType() == Lien::CROIX){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
            switch(i){
                case 2:
                    if(c.atCaseVoisin(2)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(2)->atL(1)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(2)->atL(2), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(2)->atL(1), Lien::SEGMENT);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 4:
                    if(c.atCaseVoisin(4)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(4)->atL(2)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(4)->atL(3), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(4)->atL(2), Lien::SEGMENT);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 6:
                    if(c.atCaseVoisin(6)->atL(4)->getType() == Lien::VIDE || c.atCaseVoisin(6)->atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(6)->atL(4), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(6)->atL(3), Lien::SEGMENT);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    }
                    break;
                case 8:
                    if(c.atCaseVoisin(8)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(8)->atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(8)->atL(1), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(8)->atL(4), Lien::SEGMENT);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    }
                    break;
                default :
                    return false;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseDiag33(Case &c){
    if(c.getValeurContrainte() == 3){
        int  i = 2;
        while(i <=8 && c.atCaseVoisin(i)->getValeurContrainte() != 3){
            i=i+2;
        }
        if(i<=8){
            switch(i){
                case 2:
                    if(c.atCaseVoisin(2)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(2)->atL(2)->getType() == Lien::VIDE || c.atL(3)->getType() == Lien::VIDE || c.atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(2)->atL(1), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(2)->atL(2), Lien::SEGMENT);
                        transformeLien(*c.atL(3), Lien::SEGMENT);
                        transformeLien(*c.atL(4), Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(2)->update();
                        return true;
                    }
                    break;
                case 4:
                    if(c.atCaseVoisin(4)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(4)->atL(3)->getType() == Lien::VIDE || c.atL(1)->getType() == Lien::VIDE || c.atL(4)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(4)->atL(2), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(4)->atL(3), Lien::SEGMENT);
                        transformeLien(*c.atL(1), Lien::SEGMENT);
                        transformeLien(*c.atL(4), Lien::SEGMENT); // correction c'est pas le lien 2 mais le lien 4
                        c.update();
                        c.atCaseVoisin(4)->update();
                        return true;
                    }
                    break;
                case 6:
                    if(c.atCaseVoisin(6)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(6)->atL(4)->getType() == Lien::VIDE || c.atL(2)->getType() == Lien::VIDE || c.atL(1)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(6)->atL(3), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(6)->atL(4), Lien::SEGMENT);
                        transformeLien(*c.atL(2), Lien::SEGMENT);
                        transformeLien(*c.atL(1), Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(6)->update();
                        return true;
                    }
                    break;
                case 8:
                    if(c.atCaseVoisin(8)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(8)->atL(4)->getType() == Lien::VIDE || c.atL(2)->getType() == Lien::VIDE || c.atL(3)->getType() == Lien::VIDE){
                        transformeLien(*c.atCaseVoisin(8)->atL(1), Lien::SEGMENT);
                        transformeLien(*c.atCaseVoisin(8)->atL(4), Lien::SEGMENT);
                        transformeLien(*c.atL(2), Lien::SEGMENT);
                        transformeLien(*c.atL(3), Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(8)->update();
                        return true;
                    }
                    break;
                    return true;
                default :
                    return false;
            }
        }else{
            return false;
        }
    }
    return false;
    
}

bool Resolution::CaseDiag323(Case &c){
    int j=6; //c.atCaseVoisin(j)
    for(int i=2 ; i<8; i++)
    {
        if(j>8)
        {
            j=2;
        }
        if(c.getValeurContrainte() == 2 && c.atCaseVoisin(i)->getValeurContrainte() == 3 && c.atCaseVoisin(j)->getValeurContrainte() == 3)
        {
            if(c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1){
                
                if(c.atCaseVoisin(i)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(i)->atL(4)->getType() == Lien::VIDE || c.atCaseVoisin(j)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(j)->atL(3)->getType() == Lien::VIDE){
                    transformeLien(*c.atCaseVoisin(i)->atL(1), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(i)->atL(4), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(2), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(3), Lien::SEGMENT);
                    c.update();
                    c.atCaseVoisin(i)->update();
                    c.atCaseVoisin(j)->update();
                    return true;
                }
            }if(c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1){
                
                if(c.atCaseVoisin(i)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(i)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(j)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(j)->atL(4)->getType() == Lien::VIDE){
                    transformeLien(*c.atCaseVoisin(i)->atL(2), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(i)->atL(1), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(3), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(4), Lien::SEGMENT);
                    c.update();
                    c.atCaseVoisin(i)->update();
                    c.atCaseVoisin(j)->update();
                    return true;
                }

            }if(c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1){
                
                if(c.atCaseVoisin(i)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(i)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(j)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(j)->atL(4)->getType() == Lien::VIDE){
                    transformeLien(*c.atCaseVoisin(i)->atL(3), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(i)->atL(2), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(4), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(1), Lien::SEGMENT);
                    c.update();
                    c.atCaseVoisin(i)->update();
                    c.atCaseVoisin(j)->update();
                    return true;
                }

            }if(c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1){
                
                if(c.atCaseVoisin(i)->atL(4)->getType() == Lien::VIDE || c.atCaseVoisin(i)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(j)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(j)->atL(2)->getType() == Lien::VIDE){
                    transformeLien(*c.atCaseVoisin(i)->atL(4), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(i)->atL(3), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(1), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(2), Lien::SEGMENT);
                    c.update();
                    c.atCaseVoisin(i)->update();
                    c.atCaseVoisin(j)->update();
                    return true;
                }
            }else
            {
                return false;
            }
        }
        j+=2;
        i+=1;
    }
    return false;
}

bool Resolution::CaseDiag223(Case &c){
    //i 2
    int j=6; //3
    for (int i = 2; i <= 8 ; ++i)
    {
        if(j>8)
        {
            j=2;
        }
        if(c.getValeurContrainte() == 2 && c.atCaseVoisin(i)->getValeurContrainte() == 2 && c.atCaseVoisin(j)->getValeurContrainte() == 3)
        {
            if(c.atCaseVoisin(i)->atL(4)->getType() == Lien::SEGMENT && c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1){
                
                if(c.atCaseVoisin(i)->atL(2)->getType() == Lien::VIDE || c.atCaseVoisin(i)->atL(3)->getType() == Lien::VIDE){
                    transformeLien(*c.atCaseVoisin(j)->atL(2), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(3), Lien::SEGMENT);
                    c.update();
                    c.atCaseVoisin(i)->update();
                    c.atCaseVoisin(j)->update();
                    return true;
                }
            }if(c.atCaseVoisin(i)->atL(1)->getType() == Lien::SEGMENT && c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1){
                
                if(c.atCaseVoisin(i)->atL(3)->getType() == Lien::VIDE || c.atCaseVoisin(i)->atL(4)->getType() == Lien::VIDE){
                    transformeLien(*c.atCaseVoisin(j)->atL(3), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(4), Lien::SEGMENT);
                    c.update();
                    c.atCaseVoisin(i)->update();
                    c.atCaseVoisin(j)->update();
                    return true;
                }
            }if(c.atCaseVoisin(i)->atL(2)->getType() == Lien::SEGMENT && c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1){
                
                if(c.atCaseVoisin(i)->atL(4)->getType() == Lien::VIDE || c.atCaseVoisin(i)->atL(1)->getType() == Lien::VIDE){
                    transformeLien(*c.atCaseVoisin(j)->atL(4), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(1), Lien::SEGMENT);
                    c.update();
                    c.atCaseVoisin(i)->update();
                    c.atCaseVoisin(j)->update();
                    return true;
                }
            }if(c.atCaseVoisin(i)->atL(3)->getType() == Lien::SEGMENT && c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1){
                
                if(c.atCaseVoisin(i)->atL(1)->getType() == Lien::VIDE || c.atCaseVoisin(i)->atL(2)->getType() == Lien::VIDE){
                    transformeLien(*c.atCaseVoisin(j)->atL(1), Lien::SEGMENT);
                    transformeLien(*c.atCaseVoisin(j)->atL(2), Lien::SEGMENT);
                    c.update();
                    c.atCaseVoisin(i)->update();
                    c.atCaseVoisin(j)->update();
                    return true;
                }
            }else
            {
                return false;
            }
        }
        j+=2;
        i+=1;
    }
    return false;
}

bool Resolution::CasSemiBoucle()
{
    bool res = false;
    Lien* l = parcours.lienCasSemiBoucle();
    while(l != NULL) {
        transformeLien(*l, Lien::CROIX);
        l = parcours.lienCasSemiBoucle();
        res = true;
    }
    return res;
}

bool Resolution::lienDejaUtilise(Lien& l)
{
    for(int i = 0; i < LiensUtilise.size(); i++) {
        if(LiensUtilise[i]->lienEgal(l)) {
            return true;
        }
    }
    LiensUtilise.push_back(&l);
    return false;
}
