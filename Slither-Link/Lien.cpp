//
//  Lien.cpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 16/02/2019.
//  Copyright © 2019. All rights reserved.
//

#include "Lien.hpp"

Lien::Lien():p1(NULL), p2(NULL), type(VIDE){}
Lien::Lien(Point* p1, Point* p2): p1(p1), p2(p2), type(VIDE){}
Lien::Lien(Point* p1, Point* p2, e_type type): p1(p1), p2(p2), type(type){}

std::ostream& Lien::affiche(std::ostream& os) const{
    return os<<"Lien: "<<*p1<<" "<<*p2<<" type : "<<Type2String(type)<<std::endl;
}
    
Point *Lien::atP1(void) const {
    return this->p1;
}
Point *Lien::atP2(void) const {
    return this->p2;
}
Lien::e_type Lien::getType(void) const{
    return this->type;
}

/*void Lien::setType(e_type type){
    this->type=type;
    this->atP1()->update();
    this->atP1()->reglesPoint();
    this->atP2()->update();
    this->atP2()->reglesPoint();
}*/

void Lien::setType(e_type type){
    this->type=type;
    //this->atP1()->update();
    //this->atP2()->update();
    //this->atP1()->reglesPoint();
    //this->atP2()->reglesPoint();
}

bool Lien::estVide(void) const{
    return this->type == Lien::VIDE;
}

std::ostream& operator<<(std::ostream& os, const Lien& lien){
    return lien.affiche(os);
}

bool Lien::operator==(const Lien& l) const
{
    return (*this == l);
}

bool Lien::lienEgal(Lien& l) const
{
    if(p1 == l.atP1() && p2 == l.atP2())
        return true;
    else if(p2 == l.atP1() && p1 == l.atP2())
        return true;
    else 
        return false;
}


std::string Type2String(Lien::e_type type) {
    std::string resultat;
    switch (type) {
        case Lien::SEGMENT:
            resultat = "segement";
            break;
        case Lien::CROIX:
            resultat = "croix";
            break;
        case Lien::VIDE:
            resultat = "vide";
            break;
        default:
            resultat = "Erreur : Type non définie.";
    }
    return resultat;
}
