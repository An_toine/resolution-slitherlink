//
//  CasParticulier.cpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 24/02/2019.
//  Copyright © 2019. All rights reserved.
//

#include "CasParticuliers.hpp"

using namespace std;

// DEBUT DES CAS PARTICULIERS
// NOMENCLATURE
// Case[TypeVoisinage][Chiffre1][Chiffre2]_[Numéro]
// si cas 'mirroir' : M 
// Type de voisinage:
//      Seul
//      Coin
//      Diagonale
//      Voisin (verticale/horizontale)
// --
// CASE SEUL
// -------------------------------------------------------------------

bool CaseSeul0(Case &c){
    if(c.getValeurContrainte() == 0){
        c.atL(1)->setType(Lien::CROIX);
        c.atL(2)->setType(Lien::CROIX);
        c.atL(3)->setType(Lien::CROIX);
        c.atL(4)->setType(Lien::CROIX);
        c.update();
        return true;
    }
    else
    {
        return false;
    }
}

bool CaseSeul1_1(Case &c){
    if(c.getValeurContrainte() == 1){
       if(c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP1()->atL(4)->getType() == Lien::SEGMENT){
       	 	c.atL(2)->setType(Lien::CROIX);
        	c.atL(3)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::SEGMENT && c.atL(1)->atP2()->atL(2)->getType() == Lien::CROIX){
        	c.atL(3)->setType(Lien::CROIX);
       		c.atL(4)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(2)->getType() == Lien::SEGMENT){
      	    c.atL(4)->setType(Lien::CROIX);
       	    c.atL(1)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(3)->atP1()->atL(4)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT){
        	c.atL(1)->setType(Lien::CROIX);
            c.atL(2)->setType(Lien::CROIX);
            c.update();
            return true;
        }else{
            return false;
        }
    }else{ 
        return false;
    }   
}

bool CaseSeul1_1M(Case &c){
    if(c.getValeurContrainte() == 1){
       if(c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(4)->getType() == Lien::SEGMENT){
            c.atL(1)->setType(Lien::CROIX);
            c.atL(2)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(1)->atP1()->atL(1)->getType() == Lien::SEGMENT && c.atL(1)->atP1()->atL(4)->getType() == Lien::CROIX){
            c.atL(2)->setType(Lien::CROIX);
            c.atL(3)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(2)->getType() == Lien::SEGMENT){
            c.atL(3)->setType(Lien::CROIX);
            c.atL(4)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(3)->atP2()->atL(2)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(3)->getType() == Lien::SEGMENT){
            c.atL(1)->setType(Lien::CROIX);
            c.atL(4)->setType(Lien::CROIX);
            c.update();
            return true;
        }else{
            return false;
        }
    }else{ 
        return false;
    }   
}

bool CaseSeul1_2(Case &c){
    if(c.getValeurContrainte() == 1){
       if(c.atL(1)->atP1()->atL(4)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::CROIX && c.atL(3)->getType() == Lien::CROIX){
            c.atL(1)->atP1()->atL(1)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::SEGMENT && c.atL(3)->getType() == Lien::CROIX && c.atL(4)->getType() == Lien::CROIX){
            c.atL(1)->atP2()->atL(2)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(3)->atP2()->atL(2)->getType() == Lien::SEGMENT && c.atL(4)->getType() == Lien::CROIX && c.atL(1)->getType() == Lien::CROIX){
            c.atL(3)->atP2()->atL(3)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT && c.atL(1)->getType() == Lien::CROIX && c.atL(2)->getType() == Lien::CROIX){
            c.atL(3)->atP1()->atL(4)->setType(Lien::CROIX);
            c.update();
            return true;
        }else{
            return false;
        }
    }else{ 
        return false;
    }   
}

bool CaseSeul1_2M(Case &c){
    if(c.getValeurContrainte() == 1){
       if(c.atL(3)->atP1()->atL(4)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::CROIX && c.atL(1)->getType() == Lien::CROIX){
            c.atL(3)->atP1()->atL(3)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(1)->atP1()->atL(1)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::CROIX && c.atL(3)->getType() == Lien::CROIX){
            c.atL(1)->atP1()->atL(4)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(1)->atP2()->atL(2)->getType() == Lien::SEGMENT && c.atL(3)->getType() == Lien::CROIX && c.atL(4)->getType() == Lien::CROIX){
            c.atL(1)->atP2()->atL(1)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(3)->atP2()->atL(3)->getType() == Lien::SEGMENT && c.atL(4)->getType() == Lien::CROIX && c.atL(1)->getType() == Lien::CROIX){
            c.atL(3)->atP2()->atL(2)->setType(Lien::CROIX);
            c.update();
            return true;
        }else{
            return false;
        }
    }else{ 
        return false;
    }   
}

bool CaseSeul2(Case &c){
    if(c.getValeurContrainte() == 2){
        if(c.atL(4)->atP1()->atL(4)->getType() == Lien::SEGMENT && c.atL(1)->getType() == Lien::CROIX){
            c.atL(2)->setType(Lien::SEGMENT);
            c.atL(3)->atP1()->atL(3)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(1)->atP1()->atL(1)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::CROIX){
            c.atL(3)->setType(Lien::SEGMENT);
            c.atL(1)->atP1()->atL(4)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(2)->atP1()->atL(2)->getType() == Lien::SEGMENT && c.atL(3)->getType() == Lien::CROIX){
            c.atL(4)->setType(Lien::SEGMENT); //fix position lien
            c.atL(1)->atP2()->atL(1)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT && c.atL(4)->getType() == Lien::CROIX){
            c.atL(1)->setType(Lien::SEGMENT);
            c.atL(3)->atP2()->atL(2)->setType(Lien::CROIX);
            c.update();
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

bool CaseSeul2M(Case &c){
    if(c.getValeurContrainte() == 2){
        if(c.atL(1)->atP1()->atL(4)->getType() == Lien::SEGMENT && c.atL(3)->getType() == Lien::CROIX){
            c.atL(2)->setType(Lien::SEGMENT);
            c.atL(1)->atP1()->atL(1)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::SEGMENT && c.atL(4)->getType() == Lien::CROIX){
            c.atL(3)->setType(Lien::SEGMENT);
            c.atL(1)->atP2()->atL(2)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(3)->atP2()->atL(2)->getType() == Lien::SEGMENT && c.atL(1)->getType() == Lien::CROIX){
            c.atL(4)->setType(Lien::SEGMENT);
            c.atL(2)->atP2()->atL(3)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::CROIX){
            c.atL(1)->setType(Lien::SEGMENT);
            c.atL(3)->atP1()->atL(4)->setType(Lien::CROIX);
            c.update();
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

//CASE COIN

bool CaseCoin1(Case &c){
    if(c.getValeurContrainte() == 1){
        if(c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP1()->atL(4)->getType() == Lien::CROIX){
            c.atL(1)->setType(Lien::CROIX);
            c.atL(4)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(2)->getType() == Lien::CROIX){
            c.atL(1)->setType(Lien::CROIX);
            c.atL(2)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(3)->atP2()->atL(2)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX){
            c.atL(2)->setType(Lien::CROIX);
            c.atL(3)->setType(Lien::CROIX);
            c.update();
            return true;
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(4)->getType() == Lien::CROIX){
            c.atL(3)->setType(Lien::CROIX);
            c.atL(4)->setType(Lien::CROIX);
            c.update();
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}
bool CaseCoin2(Case &c){
    if(c.getValeurContrainte() == 2){
        if(c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP1()->atL(4)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atL(4)->atP2()->atL(4)->getType() == Lien::CROIX){
            c.atL(1)->atP2()->atL(2)->setType(Lien::SEGMENT);
            c.atL(3)->atP1()->atL(3)->setType(Lien::SEGMENT);
            c.update();
            return true;
        }else if(c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atL(2)->atP1()->atL(2)->getType() == Lien::CROIX && c.atL(2)->atP2()->atL(2)->getType() == Lien::CROIX){
            c.atL(1)->atP1()->atL(4)->setType(Lien::SEGMENT);
            c.atL(3)->atP2()->atL(3)->setType(Lien::SEGMENT);
            c.update();
            return true;
        }else if(c.atL(2)->atP1()->atL(2)->getType() == Lien::CROIX && c.atL(2)->atP2()->atL(2)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX){
            c.atL(1)->atP2()->atL(1)->setType(Lien::SEGMENT);
            c.atL(3)->atP1()->atL(4)->setType(Lien::SEGMENT);
            c.update();
            return true;
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX && c.atL(4)->atP1()->atL(4)->getType() == Lien::CROIX && c.atL(4)->atP2()->atL(4)->getType() == Lien::CROIX){
            c.atL(1)->atP1()->atL(1)->setType(Lien::SEGMENT);
            c.atL(3)->atP2()->atL(2)->setType(Lien::SEGMENT);
            c.update();
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}
    
bool CaseCoin3(Case &c){
    if(c.getValeurContrainte() == 3){
        if(c.atL(1)->atP1()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP1()->atL(4)->getType() == Lien::CROIX){
            c.atL(1)->setType(Lien::SEGMENT);
            c.atL(4)->setType(Lien::SEGMENT);
            c.update();
            return true;
        }else if(c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atL(1)->atP2()->atL(2)->getType() == Lien::CROIX){
        	c.atL(1)->setType(Lien::SEGMENT);
            c.atL(2)->setType(Lien::SEGMENT);
            c.update();
            return true;
        }else if(c.atL(3)->atP2()->atL(2)->getType() == Lien::CROIX && c.atL(3)->atP2()->atL(3)->getType() == Lien::CROIX){
            c.atL(2)->setType(Lien::SEGMENT);
            c.atL(3)->setType(Lien::SEGMENT);
            c.update();
            return true;
        }else if(c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atL(3)->atP1()->atL(4)->getType() == Lien::CROIX){
            c.atL(3)->setType(Lien::SEGMENT);
            c.atL(4)->setType(Lien::SEGMENT);
            c.update();
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}
//CASE VOISIN
//---------------------------------------------------------------------

bool CaseVoisin33(Case &c){
    if(c.getValeurContrainte() == 3){
        int  i = 1;
        while(i <=7 && c.atCaseVoisin(i)->getValeurContrainte() != 3){
            i=i+2;
        }
        if(i<=7){
                switch(i){
                    case 1:
                        c.atL(1)->setType(Lien::SEGMENT);
                        c.atL(3)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(1)->atL(1)->setType(Lien::SEGMENT);
                        c.atL(1)->atP1()->atL(4)->setType(Lien::CROIX);
                        c.atL(1)->atP2()->atL(2)->setType(Lien::CROIX);
                        c.update();
                        c.atCaseVoisin(1)->update();
                        return true;
                    case 3:
                        c.atL(4)->setType(Lien::SEGMENT);
                        c.atL(2)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(3)->atL(2)->setType(Lien::SEGMENT);
                        c.atL(2)->atP1()->atL(1)->setType(Lien::CROIX);
                        c.atL(2)->atP2()->atL(3)->setType(Lien::CROIX);
                        c.update();
                        c.atCaseVoisin(3)->update();
                        return true;
                    case 5:
                        c.atL(1)->setType(Lien::SEGMENT);
                        c.atL(3)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(5)->atL(3)->setType(Lien::SEGMENT);
                        c.atL(3)->atP1()->atL(4)->setType(Lien::CROIX);
                        c.atL(3)->atP2()->atL(2)->setType(Lien::CROIX);
                        c.update();
                        c.atCaseVoisin(5)->update();
                        return true;
                    case 7:
                        c.atL(4)->setType(Lien::SEGMENT);
                        c.atL(2)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(7)->atL(4)->setType(Lien::SEGMENT);
                        c.atL(4)->atP1()->atL(1)->setType(Lien::CROIX);
                        c.atL(4)->atP2()->atL(3)->setType(Lien::CROIX);
                        c.update();
                        c.atCaseVoisin(7)->update();
                        return true;
                    default : 
                        return false;
                }
        }else{
            return false;
        }
    }else{
        return false;
    }
}

//CASE DIAGONALE
// --------------------------------------------------------------------


bool CaseDiag11_2antoine(Case &c){ // c'est quoi ?
    if(c.getValeurContrainte() == 1){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 1){
                if(i == 2 && c.atCaseVoisin(i)->atL(1)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(2)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 4 && c.atCaseVoisin(i)->atL(2)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(3)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 6 && c.atCaseVoisin(i)->atL(3)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 8 && c.atCaseVoisin(i)->atL(4)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(1)->getType() == Lien::CROIX){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
            
        }
        if(stop){
            switch(i){
                case 2:
                    c.atL(3)->setType(Lien::CROIX);
                    c.atL(4)->setType(Lien::CROIX);
                    c.update();
                    return true;
                case 4:
                    c.atL(4)->setType(Lien::CROIX);
                    c.atL(1)->setType(Lien::CROIX);
                    c.update();
                    return true;
                case 6:
                    c.atL(1)->setType(Lien::CROIX);
                    c.atL(2)->setType(Lien::CROIX);
                    c.update();
                    return true;
                case 8:
                    c.atL(2)->setType(Lien::CROIX);
                    c.atL(3)->setType(Lien::CROIX);
                    c.update();
                    return true;
                default :
                    return false;
            }
        }
    }
    return false;
}




bool CaseDiag11_2(Case &c){ //fix centré
    if(c.getValeurContrainte() == 1){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 1){
                if(i == 2 && c.atCaseVoisin(i)->atL(3)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 4 && c.atCaseVoisin(i)->atL(4)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(1)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 6 && c.atCaseVoisin(i)->atL(1)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(2)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 8 && c.atCaseVoisin(i)->atL(2)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(3)->getType() == Lien::CROIX){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
                switch(i){
                    case 2:
                        c.atL(1)->setType(Lien::CROIX);
                        c.atL(2)->setType(Lien::CROIX);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    case 4:
                        c.atL(2)->setType(Lien::CROIX);
                        c.atL(3)->setType(Lien::CROIX);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;            
                    case 6:
                        c.atL(3)->setType(Lien::CROIX);
                        c.atL(4)->setType(Lien::CROIX);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    case 8:
                        c.atL(1)->setType(Lien::CROIX);
                        c.atL(4)->setType(Lien::CROIX);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    default : 
                        return false;
                }
        }else{
            return false;
        }
    }else{
        return false;
    }
}

bool CaseDiag11_1(Case &c){ //fix -> centré
    if(c.getValeurContrainte() == 1){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 1){
                if(i == 2 && c.atCaseVoisin(i)->atL(1)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(2)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 4 && c.atCaseVoisin(i)->atL(2)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(3)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 6 && c.atCaseVoisin(i)->atL(3)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 8 && c.atCaseVoisin(i)->atL(1)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
                switch(i){
                    case 2:
                        c.atL(3)->setType(Lien::CROIX);
                        c.atL(4)->setType(Lien::CROIX);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    case 4:
                        c.atL(4)->setType(Lien::CROIX);
                        c.atL(1)->setType(Lien::CROIX);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;            
                    case 6:
                        c.atL(1)->setType(Lien::CROIX);
                        c.atL(2)->setType(Lien::CROIX);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    case 8:
                        c.atL(2)->setType(Lien::CROIX);
                        c.atL(3)->setType(Lien::CROIX);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    default : 
                        return false;
                }
        }else{
            return false;
        }
    }else{
        return false;
    }
}

bool CaseDiag12(Case &c){ //le 2 est la case centrale c 
    if(c.getValeurContrainte() == 1){ // fix -> centré
        int  i = 2;
        bool stop = false; 
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 2){
                if(i == 2 && c.atCaseVoisin(i)->atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(1)->atP2()->atL(2)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 4 && c.atCaseVoisin(i)->atL(3)->atP2()->atL(2)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(3)->atP2()->atL(3)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 6 && c.atCaseVoisin(i)->atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(3)->atP1()->atL(4)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 8 && c.atCaseVoisin(i)->atL(1)->atP1()->atL(4)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(1)->atP1()->atL(1)->getType() == Lien::SEGMENT){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
                switch(i){
                    case 2:
                        c.atL(4)->setType(Lien::CROIX);
                        c.atL(3)->setType(Lien::CROIX);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    case 4:
                        c.atL(1)->setType(Lien::CROIX);
                        c.atL(4)->setType(Lien::CROIX);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    case 6:
                        c.atL(2)->setType(Lien::CROIX);
                        c.atL(1)->setType(Lien::CROIX);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    case 8:
                        c.atL(3)->setType(Lien::CROIX);
                        c.atL(2)->setType(Lien::CROIX);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;                     
                    default : 
                        return false;
                }
        }else{
            return false;
        }
    }else{
        return false;
    }
}

bool CaseDiag22_1(Case &c){
    if(c.getValeurContrainte() == 2){
        int  i = 2;
        bool stop = false; 
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 2){
                if(i == 2 && c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT && c.atL(3)->atP1()->atL(4)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 4 && c.atL(1)->atP1()->atL(1)->getType() == Lien::SEGMENT && c.atL(1)->atP1()->atL(4)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 6 && c.atL(1)->atP2()->atL(1)->getType() == Lien::SEGMENT && c.atL(1)->atP2()->atL(2)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 8 && c.atL(3)->atP2()->atL(2)->getType() == Lien::SEGMENT && c.atL(3)->atP2()->atL(3)->getType() == Lien::SEGMENT){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
                switch(i){
                    case 2:
                        c.atL(2)->setType(Lien::SEGMENT);
                        c.atL(1)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(2)->atL(2)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(2)->atL(1)->setType(Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(2)->update();
                        return true;
                    case 4:
                        c.atL(3)->setType(Lien::SEGMENT);
                        c.atL(2)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(4)->atL(3)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(4)->atL(2)->setType(Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(4)->update();
                        return true;      
                    case 6:
                        c.atL(4)->setType(Lien::SEGMENT);
                        c.atL(3)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(6)->atL(4)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(6)->atL(3)->setType(Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(6)->update();
                        return true;
                    case 8:
                        c.atL(1)->setType(Lien::SEGMENT);
                        c.atL(4)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(8)->atL(1)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(8)->atL(4)->setType(Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(8)->update();
                        return true;
                    default : 
                        return false;
                }
        }else{
            return false;
        }
    }else{
        return false;
    }
}

bool CaseDiag22_2(Case &c){
    if(c.getValeurContrainte() == 2){
        int  i = 2;
        bool stop = false; 
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 2){
                if(i == 2 && c.atL(3)->atP1()->atL(3)->getType() == Lien::SEGMENT && c.atL(1)->atP1()->atL(4)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(1)->atP2()->atL(1)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 4 && c.atL(1)->atP1()->atL(4)->getType() == Lien::SEGMENT && c.atL(1)->atP2()->atL(1)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(3)->atP2()->atL(2)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 6 && c.atL(1)->atP2()->atL(1)->getType() == Lien::SEGMENT && c.atL(2)->atP2()->atL(2)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(3)->atP1()->atL(3)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 8 && c.atL(3)->atP2()->atL(2)->getType() == Lien::SEGMENT && c.atL(3)->atP1()->atL(3)->getType() == Lien::CROIX && c.atCaseVoisin(i)->atL(1)->atP1()->atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
                switch(i){
                    case 2:
                        c.atCaseVoisin(1)->atL(1)->atP2()->atL(2)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(1)->update();
                        c.update();
                        return true;
                    case 4:
                        c.atCaseVoisin(3)->atL(3)->atP2()->atL(3)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(3)->update();
                        c.update();
                        return true;      
                    case 6:
                        c.atCaseVoisin(5)->atL(1)->atP1()->atL(4)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(5)->update();
                        c.update();
                        return true;
                    case 8:
                        c.atCaseVoisin(7)->atL(1)->atP1()->atL(1)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(7)->update();
                        c.update();
                        return true;
                    default : 
                        return false;
                }
        }else{
            return false;
        }
    }else{
        return false;
    }
}

bool CaseDiag31_1(Case &c){
    if(c.getValeurContrainte() == 3){
        int  i = 2;
        bool stop = false;
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 1){// la case 3 est donc la case centrale...
                if(i == 2 && c.atL(4)->getType() == Lien::SEGMENT && c.atL(3)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 4 && c.atL(1)->getType() == Lien::SEGMENT && c.atL(4)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 6 && c.atL(2)->getType() == Lien::SEGMENT && c.atL(1)->getType() == Lien::SEGMENT){
                    stop = true;
                }else if(i == 8 && c.atL(3)->getType() == Lien::SEGMENT && c.atL(2)->getType() == Lien::SEGMENT){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
                switch(i){
                    case 2:
                        c.atCaseVoisin(2)->atL(1)->setType(Lien::CROIX); 
                        c.atCaseVoisin(2)->atL(2)->setType(Lien::CROIX);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    case 4:
                        c.atCaseVoisin(4)->atL(2)->setType(Lien::CROIX);
                        c.atCaseVoisin(4)->atL(3)->setType(Lien::CROIX);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;      
                    case 6:
                        c.atCaseVoisin(6)->atL(3)->setType(Lien::CROIX);
                        c.atCaseVoisin(6)->atL(4)->setType(Lien::CROIX);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    case 8:
                        c.atCaseVoisin(8)->atL(4)->setType(Lien::CROIX);
                        c.atCaseVoisin(8)->atL(1)->setType(Lien::CROIX);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    default : 
                        return false;
                }
        }else{
            return false;
        }
    }else{
        return false;
    }
}

bool CaseDiag31_2(Case &c){
    if(c.getValeurContrainte() == 1){
        int  i = 2;
        bool stop = false; 
        while(!stop && i <=8){
            if(c.atCaseVoisin(i)->getValeurContrainte() == 3){// la case 1 est donc la case centrale...
                if(i == 2 && c.atL(3)->getType() == Lien::CROIX && c.atL(4)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 4 && c.atL(4)->getType() == Lien::CROIX && c.atL(1)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 6 && c.atL(1)->getType() == Lien::CROIX && c.atL(2)->getType() == Lien::CROIX){
                    stop = true;
                }else if(i == 8 && c.atL(2)->getType() == Lien::CROIX && c.atL(3)->getType() == Lien::CROIX){
                    stop = true;
                }else{
                    i=i+2;
                }
            }else{
                i=i+2;
            }
        }
        if(stop){
                switch(i){
                    case 2:
                        c.atCaseVoisin(2)->atL(2)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(2)->atL(1)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(2)->update();
                        c.update();
                        return true;
                    case 4:
                        c.atCaseVoisin(4)->atL(3)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(4)->atL(2)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(4)->update();
                        c.update();
                        return true;      
                    case 6:
                        c.atCaseVoisin(6)->atL(4)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(6)->atL(3)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(6)->update();
                        c.update();
                        return true;
                    case 8:
                        c.atCaseVoisin(8)->atL(1)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(8)->atL(4)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(8)->update();
                        c.update();
                        return true;
                    default : 
                        return false;
                }
        }else{
            return false;
        }
    }else{
        return false;
    }
}

bool CaseDiag33(Case &c){
    if(c.getValeurContrainte() == 3){
        int  i = 2;
        while(i <=8 && c.atCaseVoisin(i)->getValeurContrainte() != 3){
            i=i+2;
        }
        if(i<=8){
                switch(i){
                    case 2:
                        c.atCaseVoisin(2)->atL(1)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(2)->atL(2)->setType(Lien::SEGMENT);
                        c.atL(3)->setType(Lien::SEGMENT);
                        c.atL(4)->setType(Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(2)->update();
                        return true;
                    case 4:
                        c.atCaseVoisin(4)->atL(2)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(4)->atL(3)->setType(Lien::SEGMENT);
                        c.atL(1)->setType(Lien::SEGMENT);
                        c.atL(4)->setType(Lien::SEGMENT); // correction c'est pas le lien 2 mais le lien 4
                        c.update();
                        c.atCaseVoisin(4)->update();
                        return true;
                    case 6:
                        c.atCaseVoisin(6)->atL(3)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(6)->atL(4)->setType(Lien::SEGMENT);
                        c.atL(2)->setType(Lien::SEGMENT);
                        c.atL(1)->setType(Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(6)->update();
                        return true;
                    case 8:
                        c.atCaseVoisin(8)->atL(1)->setType(Lien::SEGMENT);
                        c.atCaseVoisin(8)->atL(4)->setType(Lien::SEGMENT);
                        c.atL(2)->setType(Lien::SEGMENT);
                        c.atL(3)->setType(Lien::SEGMENT);
                        c.update();
                        c.atCaseVoisin(8)->update();
                        return true;
                    default : 
                        return false;
                }
        }else{
            return false;
        }
    }else{
        return false;
    }
}

bool CaseDiag323(Case &c){
    int j=6; //c.atCaseVoisin(j)
    for(int i=2 ; i<8; i++)
    {
        if(j>8)
        {
            j=2;
        }
        if(c.getValeurContrainte() == 2 && c.atCaseVoisin(i)->getValeurContrainte() == 3 && c.atCaseVoisin(j)->getValeurContrainte() == 3)
        {
            if(c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1){
                c.atCaseVoisin(i)->atL(1)->setType(Lien::SEGMENT);
                c.atCaseVoisin(i)->atL(4)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(2)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(3)->setType(Lien::SEGMENT);
                c.update();
                c.atCaseVoisin(i)->update();
                c.atCaseVoisin(j)->update();
                return true;
            }if(c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1){
                c.atCaseVoisin(i)->atL(2)->setType(Lien::SEGMENT);
                c.atCaseVoisin(i)->atL(1)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(3)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(4)->setType(Lien::SEGMENT);
                c.update();
                c.atCaseVoisin(i)->update();
                c.atCaseVoisin(j)->update();
                return true;
            }if(c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1){
                c.atCaseVoisin(i)->atL(3)->setType(Lien::SEGMENT);
                c.atCaseVoisin(i)->atL(2)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(4)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(1)->setType(Lien::SEGMENT);
                c.update();
                c.atCaseVoisin(i)->update();
                c.atCaseVoisin(j)->update();
                return true;
            }if(c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1){
                c.atCaseVoisin(i)->atL(4)->setType(Lien::SEGMENT);
                c.atCaseVoisin(i)->atL(3)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(1)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(2)->setType(Lien::SEGMENT);
                c.update();
                c.atCaseVoisin(i)->update();
                c.atCaseVoisin(j)->update();
                return true;
            }else
            {
                return false;
            }
        }
        j+=2;
        i+=1;
    }
    return false;    
}

bool CaseDiag223(Case &c){
     //i 2
    int j=6; //3
    for (int i = 2; i <= 8 ; ++i)
    {   
        if(j>8)
        {
            j=2;
        }
        if(c.getValeurContrainte() == 2 && c.atCaseVoisin(i)->getValeurContrainte() == 2 && c.atCaseVoisin(j)->getValeurContrainte() == 3)
        {
            if(c.atCaseVoisin(i)->atL(4)->getType() == Lien::SEGMENT && c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1){
                c.atCaseVoisin(j)->atL(2)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(3)->setType(Lien::SEGMENT);
                c.update();
                c.atCaseVoisin(j)->update();
                return true;
            }if(c.atCaseVoisin(i)->atL(1)->getType() == Lien::SEGMENT && c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1){
                c.atCaseVoisin(j)->atL(3)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(4)->setType(Lien::SEGMENT);
                c.update();
                c.atCaseVoisin(i)->update();
                c.atCaseVoisin(j)->update();
                return true;
            }if(c.atCaseVoisin(i)->atL(2)->getType() == Lien::SEGMENT && c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1){
                c.atCaseVoisin(j)->atL(4)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(1)->setType(Lien::SEGMENT);
                c.update();
                c.atCaseVoisin(i)->update();
                c.atCaseVoisin(j)->update();
                return true;
            }if(c.atCaseVoisin(i)->atL(3)->getType() == Lien::SEGMENT && c.atCaseVoisin(i)->getCoordonnee().getX() == c.getCoordonnee().getX() - 1 && c.atCaseVoisin(i)->getCoordonnee().getY() == c.getCoordonnee().getY() + 1 && c.atCaseVoisin(j)->getCoordonnee().getX() == c.getCoordonnee().getX() + 1 && c.atCaseVoisin(j)->getCoordonnee().getY() == c.getCoordonnee().getY() - 1){
                c.atCaseVoisin(j)->atL(1)->setType(Lien::SEGMENT);
                c.atCaseVoisin(j)->atL(2)->setType(Lien::SEGMENT);
                c.update();
                c.atCaseVoisin(i)->update();
                c.atCaseVoisin(j)->update();
                return true;
            }else
            {
                return false;
            }
        }
        j+=2;
        i+=1;
    }
    return false;
}

