//
//  main.cpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 16/02/2019.
//  Copyright © 2019. All rights reserved.
// 

#include "Grille.hpp"
#include "Case.hpp"
#include "Point.hpp"
#include "Lien.hpp"
#include "Resolution.hpp"
#include "Parcours.hpp"



int main(int argc, char *argv[]) {
    
    // ----------------------------------------------------------------------------------------
    // ------------------ Passage en ligne de commande ----------------------------------------
    // ----------------------------------------------------------------------------------------
    std::string nomFichier;
    std::string destination;
    bool activationCouleur = true;
    if(argc == 2){
        std::string fichier(argv[1]);
        destination = "../dossier-test-grilles/" + fichier + ".txt";
        nomFichier = fichier;
        activationCouleur = true;
    }else{
        nomFichier = "grilleExemple"; // nomm du fichier par défault
        destination = "../dossier-test-grilles/test1.txt"; // destination par défault
    }
    // ----------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------

    Grille grille(destination, activationCouleur);
    Parcours parcours;
    Resolution resolution(grille, parcours);
    resolution.boucleCasParticulierDebut();
    std::cout << "Lancement boucle cas particuliers généraux..." << std::endl;
    usleep(8000000);
       
    bool resultatOpL, resultatCP;
    bool bt      = false;
    bool running = true;

    do {
        resultatOpL = resolution.boucleOperationsLogiques();
        resultatCP = resolution.boucleCasParticulier();
    } while(!(resultatOpL || !(resultatCP)) && (parcours.parcoursTermine() == false));

    if(parcours.parcoursTermine() == true)
        std::cout << "La grille est résolue. ";
    else {
        bt = resolution.backTracking();
    }
    if(bt)
        std::cout << "Terminé avec BackTracking." << std::endl;
    std::cout<<std::endl<<"Grille \""<<  nomFichier <<"\" après la résolution : "<<std::endl<<grille<<std::endl;
    parcours.afficherParcours();
    
  
  return 0;
}
