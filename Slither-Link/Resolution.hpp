//
//  Resolution.hpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 20/02/2019.
//  Copyright © 2019. All rights reserved.
//

#ifndef RESOLUTION_HPP
#define RESOLUTION_HPP

#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <vector>

#include "Grille.hpp"
#include "Parcours.hpp"
//#include "CasParticuliers.hpp"

class Resolution {
private:
    Grille &grille;
    Parcours &parcours;
    int tempsAffichage;
    int tmp;
    bool affichageCoupParCoup;
    std::vector<Lien*> LiensUtilise;
    
    // attributs qui compte le nombre de tracé et les enregistres
public:
    Resolution(Grille &g, Parcours &p);
    
    void affichage(void);
    
    // méthode lancement de la résolution
    // emplacement de l'appel des cas particuliers, de la boucle logique et du back tracking
    
    // méthode Cas particuliers que l'on execute qu'une seul fois au début du programme. Ces cas particuliers sont uniquement des contraintes, les liens n'entrent pas en jeu.
    bool boucleCasParticulierDebut(void);
    
    // méthode Cas particulier contient tous les appels des cas particuliers qui fonctionne avec des liens déjà existant dans la grille
    bool boucleCasParticulier(void);
    
    // méthode règles logiques
    bool boucleOperationsLogiques(void);
    
    // méthode Cas particuliers que l'on execute plusieurs fois
    bool casParticuliersGeneral(Case& _case);
    
    // méthode back tracking
    //bool boucleBackTracking(Point &p);
    bool backTracking();
    bool erreurPoint3Segments(unsigned int id_lien_debutBT) const;
    void rembobinage(unsigned int id_lien);
    
    // méthode test fin de la résolution
    bool testFinResolution(void);
    
    // méthode qui enregistre les couple d'extremité de chaque morceaux de tracé
    
    
    //On essaye de voir si on peut mettre les méthode remplir contrainte, transformerLien et regle point dans la classe resolution pour plus de controle
    
    // retourne false si il y a une erreur
    bool erreurPoint(Point &p);
    void reglesPoint(Point &p);
    // retourne false si il y a une erreur
    bool transformeLien(Lien &l, Lien::e_type type);
    bool remplirContrainte(Case &c);
    void reglesPointSurCase(Case &c);
    
    
    
    
    // cas particulier test
    
    //case seule
    bool CaseSeul0(Case &c);
    bool CaseSeul1_1(Case &c);      // liens présents
    bool CaseSeul1_1M(Case &c);
    bool CaseSeul1_2(Case &c);      // liens présents
    bool CaseSeul1_2M(Case &c);
    bool CaseSeul2(Case &c);        // liens présents
    bool CaseSeul2M(Case &c);
    //case coin
    bool CaseCoin1(Case &c); 
    bool CaseCoin2(Case &c);
    bool CaseCoin3(Case &c);
    //case voisin
    bool CaseVoisin33(Case &c);
    //case diagonnale
    bool CaseDiag11_1(Case &c);     // liens présents
    bool CaseDiag11_2(Case &c);     // liens présents
    bool CaseDiag12(Case &c);       // liens présents
    bool CaseDiag12M(Case &c);
    bool CaseDiag22_1(Case &c);     // liens présents
    bool CaseDiag22_2(Case &c);     // liens présents
    bool CaseDiag22_2M(Case &c);
    bool CaseDiag31_1(Case &c);     // liens présents
    bool CaseDiag31_2(Case &c);     // liens présents
    bool CaseDiag33(Case &c);
    bool CaseDiag223(Case &c);      // liens présents
    bool CaseDiag323(Case &c);

    bool CaseDiag11_2antoine(Case& c);

    bool CasSemiBoucle();

    bool lienDejaUtilise(Lien& l);
    
};

#endif /* RESOLUTION_HPP */
