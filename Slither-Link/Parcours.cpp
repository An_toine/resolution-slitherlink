#include "Parcours.hpp"


using namespace std;

Parcours::Parcours(void) : longueurParcours(0) {}

int Parcours::getLongueurParcours() const
{
	return this->longueurParcours;
}

int Parcours::getNbTraces() const
{
	return int(this->parcours.size());
}

void Parcours::updateTraces(Point* p1, Point* p2)
{
	bool nouveauSegment = true;
	int i = 0;
	while(nouveauSegment == true && i < parcours.size()) {
		//afficherParcours();
		if(parcours[i].traceEgal(p1, p2)) {
			if(parcours[i].getLongueur() > 1 && parcours[i].getP1()->checkPointVoisin(parcours[i].getP2())) { // si c'est une boucle
				parcours[i].setLongueur(parcours[i].getLongueur()+1);
				parcours[i].setBoucle(true);
				parcours[i].setP1(p1);
				parcours[i].setP2(p1);
				longueurParcours++;
			}
			nouveauSegment = false;
			i = 0;
		}
		else if(parcours[i].ajouterPoint(p1, p2)) {
			nouveauSegment = false;
			longueurParcours++;
		}
		i++;
	}

	if(nouveauSegment == true && i == parcours.size()) {
		if(p1->checkPointVoisin(p2)) {
			parcours.push_back(Trace(p1, p2));
			longueurParcours++;
		}
	}
}

void Parcours::sauvegarderParcours()
{
	unsigned int j = 0;
	for(int i = 0; i < parcours.size(); ++i) {
		parcours_sauvegarde.push_back(Trace(parcours[i].getP1(), parcours[i].getP2()));
		parcours_sauvegarde[i].setLongueur(parcours[i].getLongueur());
	}
}

void Parcours::backTrackParcours()
{
	parcours = parcours_sauvegarde;
	parcours_sauvegarde.clear();
}

void Parcours::updateFusionTraces() 
{
	bool fusion = false;
	for(int i = 0; i < parcours.size(); i++) {
		for(int j = 0; j < parcours.size(); j++) {
			if(&parcours[i] != &parcours[j]) {
				if(parcours[i].getP1() == parcours[j].getP1()) {
					parcours[i].setP1(parcours[i].getP2());
					parcours[i].setP2(parcours[j].getP2());
					fusion = true;
				}
				else if(parcours[i].getP1() == parcours[j].getP2()) {
					parcours[i].setP1(parcours[i].getP2());
					parcours[i].setP2(parcours[j].getP1());
					fusion = true;
				}
				else if(parcours[i].getP2() == parcours[j].getP1()) {
					parcours[i].setP2(parcours[j].getP2());
					fusion = true;
					}
				else if(parcours[i].getP2() == parcours[j].getP2()) {
					parcours[i].setP2(parcours[j].getP1());
					fusion = true;
				}
				if(fusion) {
					parcours[i].setLongueur(parcours[i].getLongueur() + parcours[j].getLongueur());
					parcours.erase(parcours.begin() + j);
					j--;
					fusion = false;
				}
			}
		}
	}
}

bool Parcours::parcoursTermine() const
{
	if(parcours.size() == 1 && parcours[0].getBoucle())
		return true;
	else
		return false;
}

void Parcours::afficherParcours() const
{
	for(int i = 0; i < parcours.size(); i++) {
		std::cout << "Tracé n°" << i << " p1" << *parcours[i].getP1() << " p2" << *parcours[i].getP2() << " longueur: " << parcours[i].getLongueur() << std::endl;
	}
}

Lien* Parcours::lienCasSemiBoucle()
{
	if(parcours.size() > 1) {
		Point *p1, *p2;
		for(int i = 0; i < parcours.size(); i++) {
			p1 = parcours[i].getP1();
			p2 = parcours[i].getP2();
			if(parcours[i].getLongueur() > 1 && p1->checkPointVoisin2(p2) && (p1 != p2)) {
				for(int j = 1; j <= 4; j++) {
					if(p1->atL(j)->getType() == Lien::VIDE) {
						if((p1->atL(j)->atP1() == p1 && p1->atL(j)->atP2() == p2) || (p1->atL(j)->atP1() == p2 && p1->atL(j)->atP2() == p1)) {
							return p1->atL(j);
						}
					}
				}
			}
		}
	}
	return NULL;
}
Point* Parcours::retournerPointBT(int trace, int point)
{
	switch(point) {
		case 1 : return parcours[trace].getP1();
		case 2 : return parcours[trace].getP2();
	}
}


