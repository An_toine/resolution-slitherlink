var indexSectionsWithContent =
{
  0: "abcegilmnoprstuvxy",
  1: "cglprt",
  2: "cglmprt",
  3: "abcegilmnoprstu",
  4: "abcglnprstvxy",
  5: "e",
  6: "cnsv",
  7: "c",
  8: "br"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Pages"
};

