#
# Grille de test 
# 
# Ce fichier peut contenir plusieurs grilles Cas particuliers pour contrainte 1 Rotation 1 exclusivement
#

# debut de la grille et nombre de case sur un côté.
[begin-grille][12]
    contrainte{1} : (1,1)
    contrainte{1} : (5,2)
    contrainte{1} : (4,1)
    contrainte{1} : (2,3)
    contrainte{1} : (4,3)
    contrainte{1} : (5,4)
    contrainte{1} : (2,5)
    contrainte{1} : (4,12)
    contrainte{1} : (9,11)
    contrainte{1} : (12,4)
    contrainte{2} : (9,1)
    contrainte{2} : (12,1)
    contrainte{2} : (8,2)  
    contrainte{2} : (10,3)
    contrainte{2} : (9,4)
    contrainte{2} : (7,5)
    contrainte{2} : (6,8)
    contrainte{2} : (9,8)
    contrainte{2} : (5,9)
    contrainte{2} : (5,11)
    contrainte{3} : (1,7)
    contrainte{3} : (3,7)
    contrainte{3} : (4,7)
    contrainte{3} : (7,7)
    contrainte{3} : (10,7)
    contrainte{3} : (2,8)
    contrainte{3} : (8,9)
    contrainte{3} : (1,12)
    Contrainte{3} : (11,5)
    contrainte{3} : (8,12)
    contrainte{0} : (11,10)
    contrainte{0} : (12,12)
[end-grille]
