#ifndef PARCOURS_HPP
#define PARCOURS_HPP

#include <stdio.h>
#include <iostream>
#include <vector>
#include "Trace.hpp"
#include "Point.hpp"
#include "Lien.hpp"



class Parcours {
private:
	std::vector<Trace> parcours;
	std::vector<Trace> parcours_sauvegarde;
	int longueurParcours;
public:
	Parcours(void);
	/*!
     *  Renvoie la longueur totale des tracés du parcours.
     */
	int getLongueurParcours() const;
	/*!
     *  Renvoie le nombre de tracés dans le parcours.
     */
	int getNbTraces() const;
	/*!
     *  Renvoie vrai si le parcours est terminé, faux sinon.
     */
	bool parcoursTermine() const;
	/*!
     *  Ajoute un point du segment au bon tracé si le segment est dans la continuité de celui-ci.
     *  Sinon, créer un nouveau tracé et le stock dans le tableau parcours.
     */
	void updateTraces(Point* p1, Point* p2);
	/*!
     *  Fusionne les tracés qui ont une extrêmité en commun.
     */
	void updateFusionTraces();
	/*!
     *  Cas Semi boucle
     */
	Lien* lienCasSemiBoucle();
	/*!
     *	Affiche le parcours
     */
	void afficherParcours() const;

	void backTrackParcours();

	void sauvegarderParcours();

	Point* retournerPointBT(int trace, int point);
};




#endif
