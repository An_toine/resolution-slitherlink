//
//  Point.hpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 16/02/2019.
//  Copyright © 2019. All rights reserved.
//

#ifndef POINT_HPP
#define POINT_HPP

#include <stdio.h>
#include <iostream>

#include "Lien.hpp"

class Lien;

/*!
 *  \class Point
 
 *  \brief Cette class permet de définir un point à partir de deux entiers : x et y. Un point possède
 */
class Point{
private:
    int x;      ///< Coordonnée x du point
    int y;      ///< Coordonnée y du point
    Lien *l1;   ///< l1 est le lien situé au haut du point.
    Lien *l2;   ///< l2 est le lien situé à droite du point.
    Lien *l3;   ///< l3 est le lien situé en bas du point.
    Lien *l4;   ///< l4 est le lien situé à gauche du point.
    
    int nbSegment;  ///< Nombre de lien qui sont des segment.
    int nbCroix;    ///< Nombre de lien qui sont des croix.
    int nbVide;     ///< Nombre de lien vide.
    
public:
    /*!
     *  Construit le point de coordonnée (-1,-1) avec des liens NULL.
     */
    Point(void);
    /*!
     *  Construit le point de coordonnée (\e x,\e y) avec des liens NULL.
     *  \param x ...
     *  \param y ...
     */
    Point(int x, int y);
    
    /*!
     *  Affiche les coordonnées du point. L'affichage est le suivant : (x,y).
     */
    std::ostream& affiche(std::ostream&) const;
    
    
    int getNbSegment(void);  ///< Accesseur en lecture : Nombre de lien qui sont des segment.
    int getNbCroix(void);    ///< Accesseur en lecture : Nombre de lien qui sont des croix.
    int getNbVide(void);     ///< Accesseur en lecture : Nombre de lien vide.
    
    /*!
     *  Méthode qui met à jour les infos sur les liens autour du point :
     *  - \ref nbSegment;
     *  - \ref nbCroix;
     *  - \ref nbVide;
     */
    void update(void); //
    
    /*!
     *  Accesseur en lecture.
     *  \return renvoie la coordonnée x du point.
     */
    int getX(void) const;
    /*!
     *  Accesseur en lecture.
     *  \return renvoie la coordonnée y du point.
     */
    int getY(void) const;
    /*!
     *  Méthode qui permet d'accéder au lien que l'on a indiqué grâce au paramètre.
     *  \param numLien est le numéro du lien auquel on souhaite accéder.
     *  \return renvoie le pointeur sur le lien indiqué en paramètre.
     */
    Lien* atL(int numLien) const;
    
    /*!
     *  Accesseur en écriture.
     *  \param numLien numéro du lien que l'on souhaite définir.
     *  \param lien pointeur vers le lien.
     */
    void setLien(int numLien, Lien* lien);
    
    /*!
     *  Méthode qui renvoie le premier lien vide trouvé. (même contrainte dans la classe Case)
     *  \return renvoie un pointeur qui pointe vers le premier lien vide trouvé, renvoie NULL s'il n'y a plus de lien vide.
     */
    Lien* unLienVide(void);
    Lien* unLienSegment(void);
    
    /*!
     *  Méthode qui vérifie si il est possible d'appliquer une règle de point, et l'execute si c'est possible.
     */
    //void reglesPoint(void);
    
    /*!
     * Méthode qui vérifie si p est un voisin (c'est à dire à poor coord. x+1, x-1 ou y+1 y-1) du point.
     */
    bool checkPointVoisin(Point* p);   

    bool checkPointVoisin2(Point* p);
};

std::ostream& operator<<(std::ostream&, const Point&);

#endif /* POINT_HPP */
