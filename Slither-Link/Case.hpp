//
//  Case.hpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 16/02/2019.
//  Copyright © 2019. All rights reserved.
//

#ifndef CASE_HPP
#define CASE_HPP

#include <stdio.h>
#include <iostream>
#include <vector>

#include "Grille.hpp"
#include "Lien.hpp"
#include "Point.hpp"

class Grille;
class Case {
private:
    Point coordonnee;   ///< Coordonnées de la case : point situé en haut à gauche de la case.
    Lien *l1;           ///< l1 est le lien situé au haut de la case.
    Lien *l2;           ///< l2 est le lien situé à droite de la case.
    Lien *l3;           ///< l3 est le lien situé en bas de la case.
    Lien *l4;           ///< l4 est le lien situé à gauche de la case.
    
    std::vector<Case*> tabVoisin;
    
    bool contrainte;    ///< Booléen permetant de savoir si la case est vide ou est une contrainte.
    int valeur;         ///< Si la case est une contrainte alors la valeur est comprise entre 0 et 3, sinon la valeur est -1.
    
    int nbSegment;      ///< Nombre de lien qui sont des segment.
    int nbCroix;        ///< Nombre de lien qui sont des croix.
    int nbLien;         ///< Nombre de lien croix et segement.
    int nbVide;         ///< Nombre de lien vide.
    
public:
    /*!
     *  Construit une case dont les coordonnées de la case sont celle du point créer par le constructeur Point sans paramètre.
     *  Les liens de la case sont initialisé à NULL.
     */
    Case(void);
    
    /*!
     * Construit une case avec les paramètres suivant :
     * \param p coordonnee de la case
     * \param l1 Pointeur vers le lien équivalent au lien l1 de la case
     * \param l2 Pointeur vers le lien équivalent au lien l2 de la case
     * \param l3 Pointeur vers le lien équivalent au lien l3 de la case
     * \param l4 Pointeur vers le lien équivalent au lien l4 de la case
     */
    Case(Point p,Lien* l1,Lien* l2,Lien* l3,Lien* l4);
    
    void update(void);
    
    /*!
     * Met en place les voisins de la case.
     */
    void setVoisins(Grille& g);
    
    /*!
     * Permet d'accéder au voisin voulu de la case.
     *
     * numCase = 1 : haut
     * numCase = 2 : droite haut diagonale
     * numCase = 3 : droite
     * numCase = 4 : droite bas diagonale
     * numCase = 5 : bas
     * numCase = 6 : gauche bas diagonale
     * numCase = 7 : gauche
     * numCase = 8 : gauche haut diagonale
     *
     * \param numCase le numéro du voisin
     * \return pointeur vers la case
     */
    Case* atCaseVoisin(int numCase);
    
    /*!
     *  Méthode qui renvoie sur la sortie standard les coordonnées de la case ainsi que les 4 liens qui lui sont associée.
     */
    std::ostream& affiche(std::ostream&) const;
    
    /*!
     *  Méthode qui permet de connaitre la coordonnée de la case (qui est la même que le point situé en haut à gauche de la case)
     *  \return Le point qui représente la coordonnée de la case.
     */
    const Point getCoordonnee(void) const;
    
    int getNbSegment(void);      
    int getNbCroix(void);
    int getNbVide(void);
    int getNbLien(void);
    /*!
     *  Méthode qui permet d'accéder au lien que l'on a indiqué grâce au paramètre.
     *  \param numLien est le numéro du lien auquel on souhaite accéder.
     *  \return renvoie le pointeur sur le lien indiqué en paramètre.
     */
    Lien* atL(int numLien) const;
    
    /*!
     *  Méthode qui permet de savoir si la case est une contrainte ou si elle est vide.
     *  \return renvoie true si la case est une contrainte, false sinon.
     */
    bool estContrainte(void) const;
    
    /*!
     *  Accesseur en lecture de la valeur de la case contrainte.
     *  \return renvoie la valeur de la case : entre 0 et 3 si la case est une contrainte, -1 sinon.
     */
    int getValeurContrainte(void) const;
    
    /*!
     *  Méthode qui permet de transformée une case vide en une contrainte en lui attribuant une valeur comprise entre 0 et 3.
     *  \param val est la valeur qui lui sera attribué.
     */
    void setContrainte(int val);
    
    /*!
     *  Méthode qui regarde la valeur d'une contrainte et change les liens vide en croix ou segment s'il est possible de remplir la contrainte.
     *  Les calcules pour savoir si on doit remplir une contraintes sont les suivants :
     *      - si on doit transformer les liens vide en segment : (this->valeur == (nbSegment + nbVide));
     *      - si on doit transformer les liens vide en croix :   (this->valeur == 4 - (nbCroix + nbVide));
     */
    //bool remplirContrainte(void);
    
    /*!
     *  Méthode qui renvoie le premier lien vide trouvé. (même contrainte dans la classe Point)
     *  \return lien pointeur vers le lien.
     */
    Lien* unLienVide(void) const;

};
std::ostream& operator<<(std::ostream&, const Case&);

#endif /* CASE_HPP */
