## Résolution : casse tête Slither Link {#mainpage}

<a href="./notes-projet.pdf" target="_blank">Voir les notes du projet</a>

### But du casse tête :

Le but est de remplir la grille de lignes entre les points pour former une boucle unique en respectant des contraintes.



### Éléments du casse tête : 

+ **Grille :** dimension : n (coté)

+ **Point** : possède une coordonnée x et y. 
+ **Lien** : un lien est le fait d'associé deux points. Un lien est fait entre deux points seulement verticalement ou horizontalement. On a défini trois types de lien : 
  + **croix** : une croix est un lien entre deux points qui indique qu'il n'y a pas de segment entre ces deux points. 
  + **segment** : un segment est un lien entre deux points qui indique qu'il y a un segment. 
    (évident non ?)
  + **vide** : pas encore définie s'il y a un segment ou pas.
+ **Case :** Une case est identifié par 4 points. Il existe deux types de case :  
  + **case vide :** c'est une case qui ne possède pas de valeur.
  + **case contrainte** : Une contrainte a une **valeur** entre 0 et 3. La valeur indique le nombre de segments entre les points de la case de type contrainte. Une contrainte peut avoir différents états: 
    + **contrainte remplie** : lorsque le nombre de segment est égale à la contrainte.
    + **contrainte terminée** : lorsque les points autour de la contrainte sont tous reliées par des liens.
    + **contrainte enCours** : lorsque la contrainte n'est ni remplie ni terminée.



**Vocabulaire :** 

Un tracé est une suite de segment. Il faut à la fin de la résolution avoir un unique tracé et non plusieurs.



### Spécification de la grille : 


+ **grille carré** : $n*n$
  un coté contient n **points** et n-1 **contraintes**. Donc, un terrain contient $n^2$ points et $(n-1)^2$ contraintes.
+ Pour la **bordure du casse tête** : Il existe aussi des **points** qui se situe en dehors du jeu, ils sont reliés par des **croix** aux points qui sont en bordure du jeu. 

![Image bordure de la grille](./assets/bordure.png) 



Sur l'image ci-dessus, la zone bleu représente les points du jeu. Ils sont entouré par d'autre points qui forme la bordure.

### Fonctions 

**Fonctions de base :** 

+ **terminé une contrainte** : 
  si on a une contrainte remplie, on met des croix entre le reste des points qui concerne la contrainte. 

  > **Exemple :** si contrainte 0 trouvé : on met des croix entre tous les points de cette contrainte.

+ fonction qui ferme une boucle : si en fermant cette boucle toutes les contraintes ne sont pas remplie et qu'il n'existe pas un unique tracé dans le jeu alors on ne met pas un segment, mais une croix.



**Conditions sur les points :** construire les liens

+ si un **point** est entouré de  **3 croix**: 
  le dernier lien est une **croix**.
+ si un **point** est entouré de **2 croix** et  **1 segment** : 
  le dernier lien est un **segment**.
+ si un **point** à déjà  **2 segments** : 
  les autres liens sont des **croix**.



**Cas particuliers : <a href="./notes-projet.pdf" target="_blank">Voir les notes du projet</a>**

**Recherche :** 

+ On recherche des cases à modifié au alentours des cases qui on été modifier au tour précédent ainsi que les cases voisines des liens modifié. On ne va pas chercher ailleurs, cela serait inutile.

**Fonction qui teste fin du jeu :** 

- vérifie que les contraintes soient bien remplies
- vérifie qu'il y a une unique boucle : tracé cyclique



**Fonction de test sur les liens :** 

+ vérifier le lien existant ou non entre deux points.
+ pour savoir si un lien est bien verticalement ou horizontalement : calculer la longueur du lien entre les deux points : 
  - si longueur du lien = 1 alors le lien est bien fait verticalement ou horizontalement
  - si longueur du lien ≠ 1 alors le lien est incorrecte.




### Idées : 

+ liste chaînée -> chaque segment = nouvelle liste chaînée ou ajout à une liste chaînée existante, on pourra aussi fusionner 2 listes chaînée pour en former une seule.

  

+ fonction de recherche : dès qu'un point possède 4 liens croix on le supprime de la structure qui s'occupe de la recherche. Il existe toujours mais il ne serra plus selectionner pour apporter des modif au puzzle.

  

+ une fois qu'on est sur que plus une seul expression logique ne peut être faite, on lance le mode back tracking.

  




### Syntaxe fichier d'entrée


Exemple d'un fichier texte : 

```txt
# une ligne de commentaire est précédé par le symbole "#"
# différentes syntaxes possible pour la grille du puzzle
# le nombre d'espace n'a pas d'importance

# une grille est carrée, on spécifie le nombre de contrainte maximum que l'on peut mettre sur un coté

[begin-grille][5]
	# différentes syntaxe possible pour définir l'emplacement d'une contrainte
	contrainte{0} : (2x2)
	contrainte{2} : 2x2 # contrainte
	contrainte{3} : 2,2
	contrainte{1} : (2,2)
[end-grille]

[begin-grille][5]
	# différentes syntaxe possible pour définir l'emplacement d'une contrainte
	contrainte{0} : (2x2)
	contrainte{2} : 2x2
	contrainte{3} : 2,2
	contrainte{1} : (2,2)
[end-grille]
```

Dans ce fichier on place toutes les grilles des différents puzzles. Dans le code de lecture des grilles, on stockera chaque grille dans une structure de donnée : taille de la grille, nombre de contrainte, positions des contraintes dans la grille.





### Algorithme : 

**Remarque :** 

+ la fonction remplir une contrainte : transforme les liens vide en croix ou segment si c'est possible.
+ la fonction qui transforme le type d'un lien : transforme le lien puis applique la règle des points sur chacun de ces points.
+ la règle des points : transforme des liens si la condition le permet.



**En gros ce que fait notre algorithme :** 

```
Initialisation de la grille
Résolution de la grille
```



+ Initialisation de la grille : 

  ```
  lecture du fichier d'entrée
  definir une grille de la taille indiqué dans le fichier
  créer les liens qui forme la bordure de la grille (lien entre les points des cases interieur et extérieur)
  placer les contraintes dans la grille
  ```




+ Résolution de la grille : 

```
boucle pour le traitement des cas particuliers
boucle règles logique (+ les cas particuliers généraux)
boucle back tracking
algorithme résolue : afficher solution
```



+ boucle pour le traitement des cas particuliers : 

```
// premier cycle de recherche : les cas particuliers (bloc de contraintes qui suivent certaines règles)

Ce placer sur la première contrainte
boucle tant qu'on a pas finie de lire toute les contraintes
	si valeurContrainte = 0 alors
    	remplirContrainte
    sinon
		si casParticulierCaseContrainte? alors
			traitementCasParticulier( Case )
		sinon
			caseContrainteSuivante()
		fin si
	fin si
fin boucle
```



+ boucle règles logique (+ les cas particuliers généraux)

```
boucle
	si caseEstContrainte alors
		si remplirContrainte?() alors
			traitementContraite()
		sinon
			si CasParticulierGeneral?() alors
        		traitementCasParticulierGeneral()
			sinon
				caseSuivante()
			fin si
		fin si
	fin si
fin boucle
```



+ Algorithme back tracking (ce qu'a écris le prof)

```
// renvoie false si la grille n'a pas de solution (= erreur) sinon renvoie Vrai
backTracking(dr : grille) : booleen
```

```
backTracking(dr : grille) : booleen

si grille fini alors 
	return true
sinon
	li <- choisir un lien vide
	sauvGrille <- grille // sauvegarder la grille
	ok <- mettre un segment pour li (renvoie booleen) // en mettant le lien à t'on une erreur ?
	si non(ok) alors
		grille <- sauvGrille //grille revient à la sauvegarde 
		ok <- mettre une croix pour li (renvoie booleen) // en mettant le lien à t'on une erreur ?
		si non(ok) alors 
			retourne false
		sinon
			return backTracking(grille)
		fin si
	sinon 
		si backTracking(grille) alors
			return true
		sinon 
			grille <- sauvGrille //grille reviens à la sauvegarde 
			ok <- mettre une croix pour li
			si non(ok) alors 
				return false
			sinon
				return backTracking(grille)
			fin si
		fin si
	fin si
fin si
```

