#ifndef TRACE_HPP
#define TRACE_HPP

#include <stdio.h>
#include <iostream>
#include <math.h>
#include <ostream>
#include "Point.hpp"


class Trace {
private:
	Point *p1;
	Point *p2;
	int longueur;
	bool boucle;
public:
	Trace(Point* p1, Point* p2);

	int getLongueur() const;
	void setLongueur(int l);
	Point *getP1() const;
	Point *getP2() const;
	void setP1(Point* p);
	void setP2(Point* p);
	bool getBoucle() const;
	void setBoucle(bool b);
	/*!
     *  Check si le point p est égal à une des extrémités du tracé.
     */
	bool checkPointEgalExtremite(Point* p) const;
	/*!
     *  Ajoute un point à un tracé déjà existant
     */
	bool ajouterPoint(Point* pt1, Point* pt2);
	/*!
	 * True si le tracé pt1<->pt2 correspond au tracé
	 */
	bool traceEgal(Point* pt1, Point *pt2) const;
};

#endif 