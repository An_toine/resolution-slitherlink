//
//  Grille.hpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 16/02/2019.
//  Copyright © 2019. All rights reserved.
//

#ifndef GRILLE_HPP
#define GRILLE_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <exception>
#include <cstdlib>
#include <regex>

#include "Case.hpp"
#include "Lien.hpp"
#include "Point.hpp"

class Case;
class Grille {
private:
    unsigned int sizeCase;                ///< nombre de cases sur un coté (sans compter les bordures).
    unsigned int sizePoint;               ///< nombre de points sur un coté (sans compter les bordures).
    unsigned int sizeCaseWithBorder;      ///< nombre de cases sur un coté (en comptant les bordures).
    unsigned int sizePointWithBorder;     ///< nombre de points sur un coté (en comptant les bordures).
    
    unsigned int nbContrainte;        ///< nombre de contrainte.
    unsigned int nbContrainteRemplie; ///< nombre de contrainte remplie.
    
    
    // Booléens pour l'affichage de la grille
    bool afficheBordure;
    bool afficheCroix;
    bool afficheSegment;
    bool afficheCouleurs;
    
    // stockage des Cases, Liens et Points
    std::vector<std::vector<Point> > tabPoint;
    std::vector<std::vector<Lien> > tabLienHorizon;
    std::vector<std::vector<Lien> > tabLienVertical;
    std::vector<std::vector<Case> > tabCase;
    
    // Méthodes utile au constructeur
    void initTabPoint(void);            ///< Initialisation du tableau qui contient les points de la grille.
    void initTabLienVertical(void);     ///< Initialisation du tableau qui contient les liens verticaux de la grille.
    void initTabLienHorizon(void);      ///< Initialisation du tableau qui contient les liens horizontaux de la grille.
    void initTabCase(void);             ///< Initialisation du tableau qui contient les cases de la grille.
    void pointAssocieLien(void);        ///< Association des liens associé aux points.
    void caseAssocieVoisin(void);       ///< On associe les voisins d'une case à celle-ci.
    void creationBordure(void);         ///< Initialisation de la bordure de la grille.
    
    /*!
     *  Lecture d'un fichier texte qui décrit la configuration de la grille.
     * \param[in] nomFichier est le nombre de case sur un coté de la grille.
     */
    void lectureFichier(std::string nomFichier);
    /*!
     *  Nettoyage des commentaire.
     *  Cette méthode prend une ligne en entrée et transforme cette ligne pour qu'elle ne contienne plus de commentaire.
     *  Un commentaire est précédé du symbole : #
     */
    void nettoieCommentaire(std::string &line);
    /*!
     *  Cette méthode contient tous les appels de fonctions qui servent à initialiser entiermenent une grille.
     */
    void init(unsigned int size);
public:
    
    std::vector<Case*> recherche; // tableau qui contient les cases pour la recherche d'éléments pour
    
    /*!
     *  Construction de la grille de taille \a size.
     *
     *  Pour la construction de la grille on fait appel à plusieurs méthodes : \n
     *  - \ref initTabPoint()    \n
     *  - \ref initTabLienVertical() \n
     *  - \ref initTabLienHorizon()  \n
     *  - \ref initTabCase() \n
     *  - \ref pointAssocieLien()    \n
     *  - \ref creationBordure() \n
     *
     * \param[in] size est le nombre de case sur un coté de la grille.
     */
    Grille(unsigned int size);
    Grille(std::string nomFichier);
    Grille(std::string nomFichier, bool couleur);
    
    
    int size(void);
    
    /*!
     *  Affichage de la grille. La syntaxe pour l'affichage est la suivante : \n
     *    • : représente un point \n
     *    x : représente un lien de type croix \n
     *    | : représente un lien verticale de type segment \n
     *    \- : représente un lien horizontale de type segment \n
     *  \return l'affichage de la grille sur la sortie standard
     *  \bug Problème d'affichage du texte en sortie, en effet de temps en temps sur le côté droit de la grille affiché, une ou plusieurs case avec une valeur alléatoire est affiché.
     */
    std::ostream& affiche(std::ostream&) const;
    Case& operator()(int y, int x);
    
};
std::ostream& operator<<(std::ostream&, const Grille);

#endif /* GRILLE_HPP */
