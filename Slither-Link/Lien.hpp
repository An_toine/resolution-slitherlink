//
//  Lien.hpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 16/02/2019.
//  Copyright © 2019. All rights reserved.
//

#ifndef LIEN_HPP
#define LIEN_HPP

#include <stdio.h>
#include <iostream>
#include <string>

#include "Point.hpp"

class Point;
class Lien {
public:
    /**
    * \brief Type possible que peut avoir un Lien.
    */
    enum e_type{
        SEGMENT, // = 0
        CROIX, // = 1
        VIDE, // = 2
        NB_TYPE
    };
private:
    Point *p1;      ///< Pointeur vers un point.
    Point *p2;      ///< Pointeur vers un point.
    e_type type;    ///< Type du lien.
public:
    /*!
     *  Construit un lien de type vide avec deux points NULL.
     */
    Lien(void);
    /*!
     *  Construit un lien de type vide avec deux points (pointeurs).
     */
    Lien(Point* p1, Point* p2);
    /*!
     *  Construit un lien de type \e type avec deux points (pointeurs).
     */
    Lien(Point* p1, Point* p2, e_type type);
    
    /*!
     *  Affiche les coordonnées du point 1 et du point 2 ainsi que le type du lien.
     *  \return Affichage sur la sortie standard.
     */
    std::ostream& affiche(std::ostream&) const;
    
    /*!
     *  Accède au point p1.
     *  \return Pointeur p1.
     */
    Point *atP1(void) const;
    /*!
     *  Accède au point p2
     *  \return Pointeur p2
     */
    Point *atP2(void) const;
    /*!
     *  \return Le type du lien
     */
    e_type getType(void) const;
    
    /*!
     *  Méthode permettant de changer le type du lien.
     *  En changeant le type de lien, on appel la méthode update() de la classe Lien pour chaque point du lien. On appel aussi la méthode de la règle des points pour chaque point du lien.
     *  \param type Le type que l'on souhaite donnée au lien
     */
    //void setType(e_type type);
    
    void setType(e_type type);
    /*!
     *  Méthode permettant de savoir si un lien est vide ou non.
     *  \return Renvoie True si le lien est vide, False sinon.
     */
    bool estVide(void) const;

    bool lienEgal(Lien& l) const;

    bool operator==(const Lien& l) const;

};

std::ostream& operator<<(std::ostream&, const Lien&);
/*!
 *  Fonction qui permet de traduire un type qui fait partie d'une énumération, en un string.
 *  - 0 : segment;
 *  - 1 : croix;
 *  - 2 : vide.
 *  \return Renvoie un string.
 */



std::string Type2String(Lien::e_type);

#endif /* LIEN_HPP */
