//
//  Point.cpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 16/02/2019.
//  Copyright © 2019. All rights reserved.
//

#include "Point.hpp"

Point::Point(int x, int y): x(x), y(y), l1(NULL), l2(NULL), l3(NULL),l4(NULL){}
Point::Point(): x(-1), y(-1), l1(NULL), l2(NULL), l3(NULL),l4(NULL){}

std::ostream& Point::affiche(std::ostream& os) const{
    return os<<"("<<x<<","<<y<<")";
}

int Point::getNbSegment() {
    return this->nbSegment;
}
int Point::getNbCroix(){
    return this->nbCroix;
}
int Point::getNbVide(){
    return this->nbVide;
}

int Point::getX(void) const{
    return this->x;
}
int Point::getY(void) const{
    return this->y;
}

Lien* Point::atL(int numLien) const{
    switch (numLien) {
        case 1: return this->l1;
        case 2: return this->l2;
        case 3: return this->l3;
        case 4: return this->l4;
        default: return this->l1;
    }
}

void Point::setLien(int numLien, Lien* lien){
    switch (numLien){
        case 1: this->l1=lien; break;
        case 2: this->l2=lien; break;
        case 3: this->l3=lien; break;
        case 4: this->l4=lien; break;
    }
}

void Point::update(void){
    this->nbCroix = 0;
    this->nbSegment = 0;
    this->nbVide = 0;
    for(int i=1;i<=4;i++){
        if(this->atL(i) != NULL){
            switch (this->atL(i)->getType()){
                case 0: nbSegment++; break;
                case 1: nbCroix++; break;
                case 2: nbVide++; break;
                default:
                    break;
            }
        }
    }
}

Lien* Point::unLienVide(void){
    int i=0;
    while(i <= 4){
        if(this->atL(i)->getType() == Lien::VIDE){
            return this->atL(i);
        }
        i++;
    }
    return NULL;
}

Lien* Point::unLienSegment(void){
    int i=0;
    while(i <= 4){
        if(this->atL(i)->getType() == Lien::SEGMENT){
            return this->atL(i);
        }
        i++;
    }
    return NULL;
}


bool Point::checkPointVoisin(Point* p)
{
    if((p->getX() <= x+1 || p->getX() >= x-1) && (p->getY() <= y+1 && p->getY() >= y-1))
        return true;
    else
        return false;
}

bool Point::checkPointVoisin2(Point* p)
{
    if((p->getX() == x+1 || p->getX() == x-1) && p->getY() == y)
        return true;
    else if((p->getY() == y+1 || p->getY() == y-1) && p->getX() == x)
        return true;
    else
        return false;
}

/*
void Point::reglesPoint(void){
    // règle 1
    if(this->nbCroix == 3 && this->nbVide == 1){
        // alors lien vide devient lien croix
        while(this->unLienVide() != NULL){
            this->unLienVide()->setType(Lien::CROIX);
        }
    }
    
    // règle 2
    if(this->nbSegment == 2 && (this->nbVide == 2 || this->nbVide == 1)){
        // alors les deux liens vide deviennent des lien croix
        while(this->unLienVide() != NULL){
            this->unLienVide()->setType(Lien::CROIX);
        }
    }
    
    // règle 3
    if(this->nbSegment == 1 && this->nbCroix == 2 && this->nbVide == 1){
        // alors les deux liens vide deviennent des lien croix
        while(this->unLienVide() != NULL){
            this->unLienVide()->setType(Lien::SEGMENT);
        }
    }
}
*/
std::ostream& operator<<(std::ostream& os, const Point &point){
    return point.affiche(os);
}
