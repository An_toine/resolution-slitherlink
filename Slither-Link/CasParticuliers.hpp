 //
//  CasParticulier.hpp
//  Slither-Link
//

//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 24/02/2019.
//  Copyright © 2019. All rights reserved.

//

#ifndef CASPARTICULIERS__HPP
#define CASPARTICULIERS__HPP

#include <stdio.h>
#include "Case.hpp"

/*!
 *  \class CasParticuliers
 *
 *  \brief Cette classe contiendra tous les cas particuliers que nous auront besoin dans la résolution du jeu, que ce soit les cas particuliers que l'on utilisera qu'une seul fois, ou les cas particuliers réutilisable.
 *  Pour facilité la lecture de ces fonctions et pour savoir à quelle cas particulier correspond chacune d'elle, nous allons établire un système de code qui permettra de les reconnaitre.
 *  Chaque Fonction (cas particulier) ferra l'objet d'un test unitaire.
 *
 */

    //CAS PARTICULIERS 
/*!
*  Nomenclature des fonctions 
*  Case_(typeDeVoisinage)_chiffre1_chiffre2_numéro
*/
    
	//case seule
    bool CaseSeul0(Case &c);
    bool CaseSeul1_1(Case &c);      // liens présents
    bool CaseSeul1_1M(Case &c);
    bool CaseSeul1_2(Case &c);      // liens présents
    bool CaseSeul1_2M(Case &c);
    bool CaseSeul2(Case &c);        // liens présents
    bool CaseSeul2M(Case &c);
    //case coin
    bool CaseCoin1(Case &c); 
    bool CaseCoin2(Case &c);
    bool CaseCoin3(Case &c);
    //case voisin
    bool CaseVoisin33(Case &c);
        //case diagonnale
    bool CaseDiag11_1(Case &c);     // liens présents
    bool CaseDiag11_2(Case &c);     // liens présents
    bool CaseDiag12(Case &c);       // liens présents
    bool CaseDiag22_1(Case &c);     // liens présents
	bool CaseDiag22_2(Case &c);     // liens présents
	bool CaseDiag31_1(Case &c);     // liens présents
	bool CaseDiag31_2(Case &c);     // liens présents
	bool CaseDiag33(Case &c);
    bool CaseDiag223(Case &c);      // liens présents
    bool CaseDiag323(Case &c);

    bool CaseDiag11_2antoine(Case& c);

#endif /* CASPARTICULIERS__HPP */
