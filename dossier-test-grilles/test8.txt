[begin-grille][5]
    contrainte{2} : (1,1)
    contrainte{3} : (2,1)
    contrainte{3} : (5,1)
 
    contrainte{2} : (3,2)
    contrainte{2} : (4,2)
 
    contrainte{2} : (1,3)
    contrainte{3} : (4,3)
    contrainte{2} : (5,3)

    contrainte{0} : (3,4)
 
    contrainte{3} : (2,5)
    contrainte{3} : (3,5)
    contrainte{3} : (5,5)
[end-grille]