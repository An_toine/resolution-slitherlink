//
//  Case.cpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 16/02/2019.
//  Copyright © 2019. All rights reserved.
//

#include "Case.hpp"

Case::Case():
    coordonnee(Point()),
    l1(NULL),
    l2(NULL),
    l3(NULL),
    l4(NULL),
    contrainte(false),
    valeur(-1){
        for(int i=0;i<8;i++){
            tabVoisin.push_back(NULL);
        }
    }

Case::Case(Point p, Lien *l1, Lien *l2, Lien *l3, Lien *l4):
    coordonnee(p),
    l1(l1),
    l2(l2),
    l3(l3),
    l4(l4),
    contrainte(false),
    valeur(-1){
        for(int i=0;i<8;i++){
            tabVoisin.push_back(NULL);
        }
    }

void Case::setVoisins(Grille& g){
    int x = this->coordonnee.getX();
    int y = this->coordonnee.getY();
    if(x > 0 && x <= g.size()){
        if(y > 0 && y <= g.size()){
            tabVoisin[0] = &g(  x  , y-1 );     // haut
            tabVoisin[1] = &g( x+1 , y-1 );     // droite haut diagonale
            tabVoisin[2] = &g( x+1 ,  y  );     // droite
            tabVoisin[3] = &g( x+1 , y+1 );     // droite bas diagonale
            tabVoisin[4] = &g(  x  , y+1 );     // bas
            tabVoisin[5] = &g( x-1 , y+1 );     // gauche bas diagonale
            tabVoisin[6] = &g( x-1 , y   );     // gauche
            tabVoisin[7] = &g( x-1 , y-1 );     // gauche haut diagonale
        }
    }
}

Case* Case::atCaseVoisin(int numCase){
    if(numCase >= 1 && numCase <= 8){
        return tabVoisin[numCase-1];
    }else{
        std::cerr<<"Erreur : la case voisine est innexistante"<<std::endl;
        return NULL;
    }

}

void Case::update(void){
    this->nbCroix = 0;
    this->nbSegment = 0;
    this->nbVide = 0;
    for(int i=1;i<=4;i++){
        if(this->atL(i) != NULL){
            switch (this->atL(i)->getType()){
                case 0: nbSegment++; break;
                case 1: nbCroix++; break;
                case 2: nbVide++; break;
                default:
                    break;
            }
        }
    }
    this->nbLien = nbSegment+ nbCroix;
}

std::ostream& Case::affiche(std::ostream& os) const{
    return os<<" Case :"<<coordonnee<<" ";
    //return os<<"Case :"<<coordonnee<<std::endl<<" "<<*l1<<" "<<*l2<<" "<<*l3<<" "<<*l4<<std::endl;
}

const Point Case::getCoordonnee(void) const{
    return this->coordonnee;
}

int Case::getNbSegment(void){
    return this->nbSegment;
}
int Case::getNbCroix(void){
    return this->nbCroix;
}
int Case::getNbVide(void){
    return this->nbVide;
}
int Case::getNbLien(void){
    return this->nbLien;
}

Lien* Case::atL(int numLien) const{
    switch (numLien) {
        case 1: return this->l1;
        case 2: return this->l2;
        case 3: return this->l3;
        case 4: return this->l4;
        default: return NULL;
    }
}

bool Case::estContrainte(void) const{
    return this->contrainte;
}

int Case::getValeurContrainte(void) const{
    return this->valeur;
}
void Case::setContrainte(int val){
    if(this->contrainte != true){
        if(val>=0 && val<=3){
            this->contrainte = true;
            this->valeur = val;
        }else{
            std::cerr<<"Erreur : La contrainte "<<this->coordonnee<<" doit avoir une valeur comprise entre 0 et 3."<<std::endl;
        }
    }else{
        std::cerr<<"Erreur  : Il n'est pas possible de changer la valeur de la contrainte "<<this->coordonnee<<" car sa valeur est déjà définie."<<std::endl;
    }

}

/*bool Case::remplirContrainte(void){
    bool remplir = false;
    if(this->estContrainte()){
        bool remplirContrainteSegment = (this->valeur == (nbSegment + nbVide));
        if(remplirContrainteSegment){
            while(unLienVide() != NULL){
                unLienVide()->setType(Lien::SEGMENT);
            }
            remplir = true;
        }
        
        bool remplirContrainteCroix = (this->valeur == 4 - (nbCroix + nbVide));
        if(remplirContrainteCroix){
            while(unLienVide() != NULL){
                unLienVide()->setType(Lien::CROIX);
            }
            remplir = true;
        }
    }
    return remplir;
}*/

Lien* Case::unLienVide(void) const{
    int i=0;
    while(i <= 4){
        if(this->atL(i) != NULL && this->atL(i)->getType() == Lien::VIDE){
            return this->atL(i);
        }
        i++;
    }
    return NULL;
}

std::ostream& operator<<(std::ostream &os, const Case &c){
    return c.affiche(os);
}
