#include "Trace.hpp"

using namespace std;


Trace::Trace(Point* p1, Point* p2) : p1(p1), p2(p2), longueur(1), boucle(false) { }

int Trace::getLongueur() const
{
	return this->longueur;
}

void Trace::setLongueur(int l) 
{
	this->longueur = l;
}

Point *Trace::getP1() const
{
	return this->p1;
}

Point *Trace::getP2() const
{
	return this->p2;
}

void Trace::setP1(Point* p)
{
	this->p1 = p;
}

void Trace::setP2(Point* p) 
{
	this->p2 = p;
}

bool Trace::getBoucle() const
{
	return this->boucle;
}
void Trace::setBoucle(bool b)
{
	this->boucle = b;
}

bool Trace::checkPointEgalExtremite(Point* p) const
{
	if(p == p1)
		return true;
	else if(p == p2)
		return true;
	else
		return false;
}

bool Trace::ajouterPoint(Point* pt1, Point *pt2) 
{	
	if(pt1->checkPointVoisin(pt2)) {
		if(p1 == pt1) {
			p1 = pt2;
			longueur++;
			return true;
		}
		else if(p1 == pt2) {
			p1 = pt1;
			longueur++;
			return true;
		}
		else if(p2 == pt1) {
			p2 = pt2;
			longueur++;
			return true;
		}
		else if(p2 == pt2) {
			p2 = pt1;
			longueur++;
			return true;
		}
	}
	return false;
}

bool Trace::traceEgal(Point* pt1, Point *pt2) const
{
	if(p1 == pt1 && p2 == pt2)
		return true;
	else if(p2 == pt1 && p1 == pt2)
		return true;
	else 
		return false;
}

