//
//  Grille.cpp
//  Slither-Link
//
//  Created by Antoine barbier, Timoléon Demas, Matthieu Commins, Djamel Benameur, Thomas Zaragoza on 16/02/2019.
//  Copyright © 2019. All rights reserved.
//

#include "Grille.hpp"

#define CHECK_BOUND(y,x)                        \
if ((y<0 || x<0)||(y>sizeCase+1 || x>sizeCase+1)){                            \
std::cout << "Accessing a Case at (" << y << ", " << x << ")"    \
<< " out of range ... aborting" << std::endl;        \
std::terminate();                       \
}

Grille::Grille(unsigned int size){
    init(size);
}

Grille::Grille(std::string nomFichier){
    this->afficheBordure = false;
    this->afficheCroix = true;
    this->afficheSegment = true;
    this->afficheCouleurs = false;
    
    lectureFichier(nomFichier);
}

Grille::Grille(std::string nomFichier, bool couleur){
    this->afficheBordure = false;
    this->afficheCroix = true;
    this->afficheSegment = true;
    this->afficheCouleurs = couleur;
    
    lectureFichier(nomFichier);
}

void Grille::init(unsigned int size){
    this->sizeCase = size;
    this->sizePoint = size+1;
    this->sizeCaseWithBorder = size+2;
    this->sizePointWithBorder = size+3;
    
    this->initTabPoint();
    this->initTabLienVertical();
    this->initTabLienHorizon();
    this->initTabCase();
    this->creationBordure();
    this->pointAssocieLien(); // important de le mettre en dernier car il faut prendre en compte les liens créés (bordure)
    this->caseAssocieVoisin();
}

int Grille::size(void){
    return sizeCase;
}

void Grille::nettoieCommentaire(std::string &s){      // Une fonction qui permet de nettoyer et ne pas prendre
    size_t pos=s.find_first_of("#");                    // en considération les commentaires du fichier texte dans la
    s=s.substr(0,pos);                                  // recherche des chaines de caracteres
    unsigned long beg=0,end=s.size()-1;
    while(beg<end+1 && (s[beg]==' ' || s[beg]=='\t' )) beg++;
    while(end>beg-1 && (s[end]==' ' || s[beg]=='\t' )) end--;
    s=s.substr(beg,end-beg+1);
}



void Grille::lectureFichier(std::string nomFichier){
    std::ifstream fluxFichierLecture(nomFichier.c_str());
    std::string line;
    if(!fluxFichierLecture.is_open()){
        std::cerr << "Le fichier " << nomFichier << " n'a pas pu être ouvert." << std::endl;
        std::terminate();
    }else{
        bool debutGrille=false;
        while(!fluxFichierLecture.eof()){
            getline(fluxFichierLecture,line);
            nettoieCommentaire(line);
            std::cmatch infos, tailleGrille;    // infos contient notre contrainte + la valeur des coordonnées x et y
            // tailleGrille récupère la taille de la grille de notre fichier texte
            std::regex debut("\\[begin-grille\\]\\[(\\d+)\\]");
            std::regex contrainte("contrainte\\{(\\d)\\} : \\(?(\\d+)[,x](\\d+)\\)?");
            std::regex fin("\\[end-grille\\]");
            if(!debutGrille){
                if(std::regex_search(line.c_str(),tailleGrille,debut)){ // Recherche de la chaine debut qui contient [begin-grille]
                    debutGrille=true;
                    this->init(stoi(tailleGrille[1]));
                    
                }
            }else{
                if(std::regex_search(line.c_str(),infos,contrainte)){ // Recherche de la chaine contrainte qui contient la contrainte
                    int valeur = stoi(infos[1]);                        // plus la valeur de x et y
                    int x = stoi(infos[2]);
                    int y = stoi(infos[3]);
                    tabCase[y][x].setContrainte(valeur);    //placer notre x et y dans leurs postions pour un affichage selon le sens
                    recherche.push_back(&tabCase[y][x]);
                }else{                                               //volue et à la posiition (y,x) notre contrainte
                    if(std::regex_match(line.c_str(),fin)){            // Recherche de l'indice de fin [end-grille] de notre fichier texte
                        debutGrille=false;
                    }
                }
            }
        }
    }
    fluxFichierLecture.close();
}


Case& Grille::operator()(int x, int y){
    CHECK_BOUND(y,x);
    return this->tabCase[y][x];
}

std::ostream& Grille::affiche(std::ostream& os) const{
    // Pour choisir d'afficher ou non la bordure : changer le booléen
    std::string strPoint = (this->afficheCouleurs ?"•": "•");
    std::string strLienSegmentV = (this->afficheCouleurs ?"\033[33m|\033[0m": "|");
    std::string strLienSegmentH = (this->afficheCouleurs ?"\033[33m-\033[0m": "-");
    std::string strLienCroix = (this->afficheCouleurs ?"\033[31mx\033[0m": "x");
    std::string strLienVide = " ";
    
    std::ostream& resultat = os;
    resultat<<" Affichage :"    <<std::endl;
    resultat<<" - Bordure : "   <<(this->afficheBordure ? "Oui" : "Non")<<std::endl;
    resultat<<" - Croix   : "   <<(this->afficheCroix   ? "Oui" : "Non")<<std::endl;
    resultat<<" - Segment : "   <<(this->afficheSegment ? "Oui" : "Non")<<std::endl;
    resultat<<std::endl;
    
    int i = (this->afficheBordure ? 0 : 1);
    int y = i;
    while(y<this->sizePointWithBorder - i){
        if(y != this->sizePointWithBorder){
            for(int x=i; x<this->sizeCaseWithBorder - i ;x++){
                resultat <<" "<<strPoint<<" ";
                resultat<<((this->tabLienHorizon[y][x].getType()==0 && this->afficheSegment)? strLienSegmentH : ((this->tabLienHorizon[y][x].getType()==1 && this->afficheCroix)? strLienCroix : strLienVide));
            }resultat << " "<<strPoint<<std::endl;
            
        }
        if(y != this->sizeCaseWithBorder - i){
            for(int x=i; x<sizePointWithBorder - i ; x++){
                resultat <<" ";
                resultat<< ((this->tabLienVertical[y][x].getType()==0 && this->afficheSegment)? strLienSegmentV : ((this->tabLienVertical[y][x].getType()==1 && this->afficheCroix)? strLienCroix : strLienVide));
                resultat<<" ";
                resultat<<(this->tabCase[y][x].estContrainte() ? std::to_string(this->tabCase[y][x].getValeurContrainte()):" ");
            }resultat <<std::endl;
        }
        y++;
    }
    return resultat;
}



   // ----------------------------------------- \\
  // ------ Les méthodes d'initialisation ------ \\
 // ---- des tableaux qui forment la grille ----- \\
//------------------------------------------------ \\



void Grille::initTabPoint(void){
    for(int y=0;y<sizePointWithBorder;y++){
        this->tabPoint.push_back(std::vector<Point>(sizePointWithBorder));
        for(int x=0;x<sizePointWithBorder;x++){
            this->tabPoint[y][x] = Point(x,y);
        }
    }
}
void Grille::initTabLienVertical(void){
    for(int y=0;y<sizeCaseWithBorder;y++){
        this->tabLienVertical.push_back(std::vector<Lien>(sizePointWithBorder));
        for(int x=0;x<sizePointWithBorder;x++){
            this->tabLienVertical[y][x] = Lien(&tabPoint[y][x],&tabPoint[y+1][x]);
        }
    }
}

void Grille::initTabLienHorizon(void){
    for(int y=0;y<sizePointWithBorder;y++){
        this->tabLienHorizon.push_back(std::vector<Lien>(sizeCaseWithBorder));
        for(int x=0;x<sizeCaseWithBorder;x++){
            this->tabLienHorizon[y][x] = Lien(&tabPoint[y][x],&tabPoint[y][x+1]);
        }
    }
}

void Grille::initTabCase(void){
    for(int y=0;y<sizeCaseWithBorder;y++){
        this->tabCase.push_back(std::vector<Case>(sizeCaseWithBorder));
        for(int x=0;x<sizeCaseWithBorder;x++){
            this->tabCase[y][x] = Case(tabPoint[y][x], &tabLienHorizon[y][x], &tabLienVertical[y][x+1], &tabLienHorizon[y+1][x], &tabLienVertical[y][x]);
        }
    }
}

void Grille::pointAssocieLien(void){
    for(int y=1;y<sizeCaseWithBorder;y++){
        for(int x=1;x<sizeCaseWithBorder;x++){
            this->tabPoint[y][x].setLien(1, &tabLienVertical[y-1][x]);
            this->tabPoint[y][x].setLien(2, &tabLienHorizon[y][x]);
            this->tabPoint[y][x].setLien(3, &tabLienVertical[y][x]);
            this->tabPoint[y][x].setLien(4, &tabLienHorizon[y][x-1]);
            this->tabPoint[y][x].update();
        }
    }
}

void Grille::caseAssocieVoisin(void){
    for(int y=1;y<=sizeCase;y++){
        for(int x=1;x<=sizeCase;x++){
            this->tabCase[y][x].setVoisins(*this);
        }
    }
}

void Grille::creationBordure(void){
    int x=0;
    //bordure haut
    for(int i=0;i<this->sizeCaseWithBorder;i++){
        this->tabCase[x][i].atL(1)->setType(Lien::CROIX);
        this->tabCase[x][i].atL(2)->setType(Lien::CROIX);
    }
    //bordure droite
    for(int i=0;i<this->sizeCaseWithBorder;i++){
        this->tabCase[i][x].atL(1)->setType(Lien::CROIX);
        this->tabCase[i][x].atL(3)->setType(Lien::CROIX);
        this->tabCase[i][x].atL(4)->setType(Lien::CROIX);
    }
    x = sizeCaseWithBorder-1;
    //bordure bas
    for(int i=0;i<this->sizeCaseWithBorder;i++){
        this->tabCase[x][i].atL(3)->setType(Lien::CROIX);
        this->tabCase[x][i].atL(2)->setType(Lien::CROIX);
        this->tabCase[x][i].atL(4)->setType(Lien::CROIX);
    }
    //bordure gauche
    for(int i=0;i<this->sizeCaseWithBorder;i++){
        this->tabCase[i][x].atL(1)->setType(Lien::CROIX);
        this->tabCase[i][x].atL(2)->setType(Lien::CROIX);
    }
}

std::ostream& operator<<(std::ostream& os, const Grille grille){
    return grille.affiche(os);
}
